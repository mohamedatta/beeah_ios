//
//  Constants.swift
//  Beeah
//
//  Created by cloudymac on 6/14/17.
//  Copyright © 2017 cloudymac. All rights reserved.
//

import Foundation

class Constants: NSObject {
    
  public static var serviceUrl = "https://beeahinternalapplication.appspot.com/"
     public static var serviceurl2  =   "https://beeahinternalapplication.appspot.com/api/v1"

    public static var profilePicData: Data? = nil
    public static var happinessStr: String = "50% happiness"
    public static var happinessVal: Int = 0
    public static var tabbarHeight: Float = 0.0
    public static func getCornerRadiusOfButton(hight: Float , width: Float) ->Float
     {
        let h = hight / 2
        let w = width / 2
        return (sqrt((h*h) + (w*w)) - w)
        
    }
    
    
    public static func getindexOfCharInString(nth: Int , str: String , ch: Character)-> Int
    {
        var counter: Int = 0
       
        for (index, char) in str.characters.enumerated() {
            if(char ==  ch)
            {
            counter+=1
            }
            if(counter == nth){
                return index}
            
            
        }
        
        
    return -1
    }



    public static  func getTimeInterval(objDate: String , format: String) -> Int64
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format //"yyyy-MM-dd HH:mm:ss.SSS"
        let date = dateFormatter.date(from: objDate)
    
    return Int64((date?.timeIntervalSince1970)!)
    }
    
    public static  func getTimeAgo(timeinterval: Int64 , objDate: String , format: String) -> String
    {
    

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format //"yyyy-MM-dd HH:mm:ss.SSS"
        let date = dateFormatter.date(from: objDate)
        let calendar = Calendar.current
    
        let objectDate =  calendar.dateComponents([.day , .month , .year , .hour , .minute , .second ], from: date!)
        
        
     let currentDate =  calendar.dateComponents([.day , .month , .year , .hour , .minute , .second ], from: Date())

        
        //Int64((date?.timeIntervalSince1970)!) - Int64(Date().timeIntervalSince1970)
        var timeDiff =   timeinterval / 1000 - Int64(Date().timeIntervalSince1970)
        timeDiff = abs(timeDiff)
        

        let nyears = timeDiff / 31556926  // nseconds in year
        
        if(nyears >= 1)
        {
            return "\(Int(nyears)) year ago."
            
        }
        
      timeDiff = timeDiff % 31556926
        
        let nmonths = timeDiff / 2629743 // n seconds in month
        if(nmonths >= 1)
        {
            return "\(Int(nmonths)) month ago."
            
        }
        
        timeDiff = timeDiff %  2629743
        
        let nweeks = timeDiff / 604799 // nseconds in week
        
        if(nweeks >= 1)
        {
            return "\(Int(nweeks)) week ago."
            
        }
        
        timeDiff = timeDiff %  604799
        
        let ndays = timeDiff / 86400 // nseconds in day
        
        if(ndays >= 1)
        {
            return "\(Int(ndays)) day ago."
            
        }
        
        timeDiff = timeDiff %  86400
        
        let nhours = timeDiff / 3600 // nseconds in hour
        if(nhours >= 1)
        {
            return "\(Int(nhours)) hour ago."
            
        }
        
        timeDiff = timeDiff %  3600
        
        
        let nminutes = timeDiff / 60 // nseconds in minute
        
        if(nminutes >= 1)
        {
            return "\(Int(nminutes)) minute ago."
            
        }
        timeDiff = timeDiff %  60
        
        return "Seconds ago!"
        

        
        
    
    }
    
    public static  func getDateTimeShortCut(timeInterval: Int64) -> String
    {
        

        
        let date = //Date.init(timeIntervalSince1970: TimeInterval(timeInterval))
        Date.init(timeIntervalSinceNow: TimeInterval(timeInterval))
        
        
        let calendar = Calendar.current

        
        
        let objectDate =  calendar.dateComponents([.day , .month , .year , .hour , .minute , .second ], from: date)
        
       
        
        
        let currentDate =  calendar.dateComponents([.day , .month , .year , .hour , .minute , .second ], from: Date())
        print(currentDate.hour!)
        
        if(currentDate.year! - objectDate.year! > 0 )
        {
            return "\(currentDate.year! - objectDate.year!) year ago"
        }
        else if(currentDate.month! - objectDate.month! > 0 )
        {return "\(currentDate.month! - objectDate.month!) month ago "}
        else if(currentDate.day! - objectDate.day! > 0 )
        {return "\(currentDate.day! - objectDate.day!) day ago "}
        else if(currentDate.hour! - objectDate.hour! > 0 )
        {return "\(currentDate.hour! - objectDate.hour!) hour ago "}
        else if(currentDate.minute! - objectDate.minute! > 0 )
        {return "\(currentDate.minute! - objectDate.minute!) minute ago "}
        else
        {return "seconds ago "}
        
        
        
        
        
        return ""
        
        
    }
    



}
