//
//  Like.swift
//  Beeah
//
//  Created by cloudymac on 6/18/17.
//  Copyright © 2017 cloudymac. All rights reserved.
//

import Foundation
class Like: NSObject {
    var type: String = "like"
    var date: String = ""
    var user: UserReference? = nil
    init(aDict: [String: AnyObject]) {
        self.type = aDict["type"] as! String
        self.date = aDict["date"] as! String
        self.user = UserReference(aDict: aDict["userReference"] as! [String : AnyObject])
        
        
    
    }
}
