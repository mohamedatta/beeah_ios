//
//  Announcements.swift
//  Beeah
//
//  Created by cloudymac on 6/18/17.
//  Copyright © 2017 cloudymac. All rights reserved.
//

import Foundation
import Firebase
class Announcment: NSObject {
    
    var title: String = ""
    var text: String = ""
    var nlikes: Int = 0
    var ndislikes: Int = 0
    var date: String = ""
    var sortDate = Int64()
    var id: String = ""
    var admin: UserReference?
    var likes = [Like]()
    
    /// cell expanded view state
    
    var isExpanded = false
    
    // like or dislike state
    var userLikeType  = ""
    
    init(aDict: [String: AnyObject]) {
        self.title = aDict["title"] as! String
       if let t = (aDict["text"] as? String)
       {
        self.text = t
        }
        
        if let nl = (aDict["nlikes"] as? Int)
        {
            self.nlikes = nl
        }
        
        if let ndl = (aDict["ndislikes"] as? Int)
        {
            self.ndislikes = ndl
        }
        
        if let t = (aDict["date"] as? String)
        {
            self.date = t
        }
        
       
        if let t = (aDict["key"] as? String)
        {
            self.id = t
        }
        
        
        if let  adminusr    =  aDict["admin"] as? [String : AnyObject]
        {
        self.admin =  UserReference(aDict:adminusr)
        }
        if let likesDict = aDict["likes"] as? [String: Any] {
            
            if let isuserlike = likesDict["\((Auth.auth().currentUser?.uid)!)"] as? [String : Any]
           {
            userLikeType = isuserlike["type"] as! String
            }
            
            for like in likesDict{
                
                self.likes.append(Like(aDict: like.value as! [String : AnyObject]))
                
            }

        }
        
        
    
    }
    
    
    
}
