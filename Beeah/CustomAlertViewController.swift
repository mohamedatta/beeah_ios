//
//  CustomAlertViewController.swift
//  Beeah
//
//  Created by cloudymac on 6/9/17.
//  Copyright © 2017 cloudymac. All rights reserved.
//

import UIKit
import Firebase
class CustomAlertViewController: UIViewController {

    @IBOutlet weak var moodssegments: UISegmentedControl!
    @IBOutlet weak var viewmid3: UIView!
    @IBOutlet weak var btn5: UIButton!
    @IBOutlet weak var btn4: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var sendBtn: UIButton!
    
    @IBOutlet weak var viewbottom: UIView!
    @IBOutlet weak var viewmid1: UIView!
    @IBOutlet weak var viewmid2: UIView!
    @IBOutlet weak var viewtop: UIView!
    @IBOutlet weak var question: UILabel!
    var questionText: String = "Test"
    var questionId: String = "id"
    
    var happiness:Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        print("mohamed")
        question.text = questionText
        
        
        
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        var image = UIImage.init(named: "moods/1small")
//        image = CustomAlertViewController.resizeImage(image: image!, withSize: CGSize(width: btn1.frame.width*2/3, height: btn1.frame.height*2/3))
        btn1.setImage( image, for:.normal)
        
        image = UIImage.init(named: "moods/2small")
        //image = CustomAlertViewController.resizeImage(image: image!, withSize: CGSize(width: btn2.frame.width*7/8, height: btn2.frame.height*7/8))
        btn2.setImage( image, for:.normal)
        
        image = UIImage.init(named: "moods/3small")
 //       image = CustomAlertViewController.resizeImage(image: image!, withSize: CGSize(width: btn3.frame.width*7/8, height: btn3.frame.height*7/8))
        btn3.setImage( image, for:.normal)
        
        image = UIImage.init(named: "moods/4small")
 //       image = CustomAlertViewController.resizeImage(image: image!, withSize: CGSize(width: btn4.frame.width*7/8, height: btn4.frame.height*7/8))
        btn4.setImage( image, for:.normal)
        
        image = UIImage.init(named: "moods/5small")
//        image = CustomAlertViewController.resizeImage(image: image!, withSize: CGSize(width: btn5.frame.width*7/8, height: btn5.frame.height*7/8))
        btn5.setImage( image, for:.normal)
        
        
        
        

    }
    @IBAction func submitBtn(_ sender: Any) {
       
        print("submit")
        if(happiness != 0)
        {
        answerQuestion(happinessLevel: happiness)
        }
    }
    @IBAction func moodSelected(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex
        {
        case 0:
      print("")
        default:
            break; 
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func btn5Action(_ sender: Any) {
    
        changeButtonsToDefault(selected: happiness)
        btn5.setImage( UIImage.init(named: "moods/5"), for:.normal)
    
        happiness=5
    }
    @IBAction func btn4Action(_ sender: Any) {
        changeButtonsToDefault(selected: happiness)

        btn4.setImage( UIImage.init(named: "moods/4"), for:.normal)

        happiness=4
    }

    @IBAction func btn3Action(_ sender: Any) {
        changeButtonsToDefault(selected: happiness)

        btn3.setImage( UIImage.init(named: "moods/3"), for:.normal)

    happiness=3
    
    }
    @IBAction func btn2Action(_ sender: Any) {

        changeButtonsToDefault(selected: happiness)

        btn2.setImage( UIImage.init(named: "moods/2"), for:.normal)

        happiness=2
    }
    @IBAction func btn1Action(_ sender: Any) {
        changeButtonsToDefault(selected: happiness)
    
        
    btn1.setImage( UIImage.init(named: "moods/1"), for:.normal)
        happiness=1

    }

    func changeButtonsToDefault(selected : Int)
    {
        sendBtn.isEnabled = true
      
        if(selected==1)
        {
            
            var image = UIImage.init(named: "moods/1small")
 //              image = CustomAlertViewController.resizeImage(image: image!, withSize: CGSize(width: btn1.frame.width/2, height: btn1.frame.height/2))
                
            btn1.setImage(image, for: .normal)
        }
        else if(selected==2)
        {
            var image = UIImage.init(named: "moods/2small")
       //     image = CustomAlertViewController.resizeImage(image: image!, withSize: CGSize(width: btn2.frame.width*2/3, height: btn2.frame.height*2/3))
        
            btn2.setImage(image, for: .normal)
        }
        else if(selected==3)
        {
            var image = UIImage.init(named: "moods/3small")
     //       image = CustomAlertViewController.resizeImage(image: image!, withSize: CGSize(width: btn3.frame.width*2/3, height: btn3.frame.height*2/3))
       
            btn3.setImage(image, for: .normal)
        }
        else if(selected==4)
        {                   var image = UIImage.init(named: "moods/4small")
   //         image = CustomAlertViewController.resizeImage(image: image!, withSize: CGSize(width: btn4.frame.width*2/3, height: btn4.frame.height*2/3))
         
            btn4.setImage(image, for: .normal)
        }
        else if(selected==5)
        {             var image = UIImage.init(named: "moods/5small")
 //           image = CustomAlertViewController.resizeImage(image: image!, withSize: CGSize(width: btn5.frame.width*2/3, height: btn5.frame.height*2/3))
         
            btn5.setImage(image, for: .normal)
        }
        
    
    }
    
    
    func answerQuestion(happinessLevel: Int)
    {
        
        if(happinessLevel == 0)
        {return}
        
        
        self.dismiss(animated: true, completion: nil)
        let currentUser = Auth.auth().currentUser
        currentUser?.getTokenForcingRefresh(true) {idToken, error in
            if let error = error {
                // Handle error
                return;
            }
            print("verified")
            print(idToken!)
            var url="\(Constants.serviceurl2)/beeah/question/answerquestion?"
            url.append("usertokenid=\(idToken!)&choice=\(happinessLevel - 1)&questionid=\(self.questionId)")
            
            
            var obj = httpRequestController();
            
            
            var response =  obj.request(url: url,type: "Get" ,completion: { (data )-> Void in
                
                print("ok")
                print(data)
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                if let dictionary = json as? [String: Any] {
                    
                    if let bool = dictionary["success"] as? Bool {
                        
                        print(bool)
                        
                        
                        if(bool == true)
                        {
                        }
                    }
                }
                
            })
            
            print(response)
            
            
        }

        
        
        
    }
    
    
    
   public static func resizeImage(image: UIImage, withSize: CGSize) -> UIImage {
        
        var actualHeight: CGFloat = image.size.height
        var actualWidth: CGFloat = image.size.width
        let maxHeight: CGFloat = withSize.width
        let maxWidth: CGFloat = withSize.height
        var imgRatio: CGFloat = actualWidth/actualHeight
        let maxRatio: CGFloat = maxWidth/maxHeight
        let compressionQuality = 0.5//50 percent compression
        
        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if(imgRatio < maxRatio) {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            } else if(imgRatio > maxRatio) {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            } else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        
        let rect: CGRect = CGRect(x: 0.0, y: 0.0, width: actualWidth, height: actualHeight)
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let image: UIImage  = UIGraphicsGetImageFromCurrentImageContext()!
        let imageData = UIImageJPEGRepresentation(image, CGFloat(compressionQuality))
        UIGraphicsEndImageContext()
        
        let resizedImage = UIImage(data: imageData!)
        return resizedImage!
        
    }

}
