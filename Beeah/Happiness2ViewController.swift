//
//  Happiness2ViewController.swift
//  Beeah
//
//  Created by Mohamed on 7/18/17.
//  Copyright © 2017 cloudymac. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import SDCAlertView
import Foundation

class Happiness2ViewController: UIViewController , TabBarSwitcher {
    @IBOutlet var happinesslabel:UILabel!
    @IBOutlet var happinessEmoji:UIImageView!
    @IBOutlet weak var CMonth: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        changeUserHappiness()
        initSwipe(direction: .left)
        initSwipe(direction: .right)
        
        let calendar = Calendar.current
        
        let currentDate =  calendar.dateComponents([ .month ], from: Date())
        
  
         CMonth.text = "\(TobContibutersViewController.months[currentDate.month! - 1])"
        
        // Do any additional setup after loading the view.
    }

    func changeUserHappiness() {
    self.happinesslabel.text = "\(Constants.happinessVal) %"
    
    let ch = Constants.happinessVal
    
    if ch <= 20 {
    self.happinessEmoji.image = UIImage(named: "veryBad.png")
    } else if (ch > 20 && ch <= 40) {
    self.happinessEmoji.image = UIImage(named: "meh.png")
    }
    else if (ch > 40 && ch <= 60) {
    self.happinessEmoji.image = UIImage(named: "satisfied.png")
    }
    else if (ch > 60 && ch <= 80) {
    self.happinessEmoji.image = UIImage(named: "happy.png")
    }
    else if (ch > 80 && ch <= 100) {
    self.happinessEmoji.image = UIImage(named: "veryHappy.png")
    }
    else {
    self.happinessEmoji.image = UIImage(named: "veryBad.png")
    }
    
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    

    
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.visibleViewController?.title = "Happiness Meter"
        
    }
   
    func handleSwipes(sender:UISwipeGestureRecognizer) {
        if (sender.direction == .left) {
            tabBarController?.selectedIndex += 1
            
        }
        else if (sender.direction == .right) {
            tabBarController?.selectedIndex -= 1
            
        }
        
        
        
    }



}
