//
//  TabBarSwitcher.swift
//  Beeah
//
//  Created by cloudymac on 7/4/17.
//  Copyright © 2017 cloudymac. All rights reserved.
//

import Foundation
import UIKit
@objc protocol TabBarSwitcher {
    func handleSwipes(sender:UISwipeGestureRecognizer)
    
}

extension TabBarSwitcher where Self: UIViewController {
    func initSwipe( direction: UISwipeGestureRecognizerDirection){
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(TabBarSwitcher.handleSwipes(sender:)))
        swipe.direction = direction
        self.view.addGestureRecognizer(swipe)
    }
}
