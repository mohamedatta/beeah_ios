//
//  HappinessViewController.swift
//  Beeah
//
//  Created by Mostafa Shuman on 7/1/17.
//  Copyright © 2017 cloudymac. All rights reserved.
//

import UIKit
import Charts
import Firebase

class HappinessViewController: UIViewController  , ChartViewDelegate , TabBarSwitcher{
   // let months = ["Jan" , "Feb", "Mar", "Apr", "May", "June", "July", "August", "Sept", "Oct", "Nov", "Dec"]
    let months = ["1" , "2" , "3" , "4"  , "5" , "6" , "7" , "8" , "9" , "10" , "11" , "12" ]
    var dollars = [0.0,0.0,0,0.0,0.0,0,11.0,0.0,0.0,0,0,0]
    var networkConnection : Reachability!
    var dollars1 = [Happiness]()
//    var happinessData: [String:Int] = [:]
    @IBOutlet weak var lineChartView: LineChartView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        requestHappiness()
        initSwipe(direction: .left)
        initSwipe(direction: .right)
        
        // Do any additional setup after loading the view.
        // 1
        self.lineChartView.delegate = self
        // 2
        self.lineChartView.chartDescription?.text = "Tap node for details"
        // 3
        self.lineChartView.chartDescription?.textColor = UIColor.white
        self.lineChartView.gridBackgroundColor = UIColor.darkGray
        // 4
        self.lineChartView.noDataText = "No data provided"
        // 5

        setChartData(months: months,values: dollars)
    
        lineChartView.isUserInteractionEnabled = false
        lineChartView.xAxis.labelPosition = .bottom
        lineChartView.xAxis.labelCount = months.count
        lineChartView.xAxis.gridLineWidth = 1
        lineChartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: months)
        lineChartView.drawBordersEnabled = true
        lineChartView.borderColor = UIColor.lightGray
        lineChartView.leftAxis.drawLabelsEnabled = false
        lineChartView.rightAxis.drawLabelsEnabled = false
        lineChartView.xAxis.granularityEnabled = true
        lineChartView.xAxis.granularity = 1.0
        lineChartView.xAxis.decimals = 0
        
        requestHappiness()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.visibleViewController?.title = "Happiness Meter"
        
    }
    
    func setChartData(months : [String], values : [Double]) {
        
        
        var dataEntries: [ChartDataEntry] = [ChartDataEntry]()
        
        for i in 0..<months.count {
            let dataEntry = ChartDataEntry(x: Double(i), y: values[i])
            print(values[i])
            dataEntries.append(dataEntry)
        }
        
        let lineChartDataSet = LineChartDataSet(values: dataEntries, label: "Months")
        
        lineChartDataSet.axisDependency = .left // Line will correlate with left axis values
        lineChartDataSet.setColor(UIColor(red: 0.0/255.0, green: 149.0/225.0, blue: 200.0/255.0, alpha: 1.0).withAlphaComponent(1.0))
        lineChartDataSet.lineWidth = 4.0
        lineChartDataSet.circleRadius = 0.0 // the radius of the node circle
        lineChartDataSet.fillAlpha = 65 / 255.0
        lineChartDataSet.highlightColor = UIColor.white
        lineChartDataSet.drawCircleHoleEnabled = false
        
        //        lineChartDataSet.
        
        
        var dataSets = [IChartDataSet]()
        dataSets.append(lineChartDataSet)
        
        let lineChartData = LineChartData(dataSets: dataSets)
        
        lineChartView.data = lineChartData
        self.lineChartView.data?.setDrawValues(false)
        
    }
    
    func requestHappiness()
    {
        
        
        let currentUser = Auth.auth().currentUser
        currentUser?.getTokenForcingRefresh(true) {idToken, error in
            if error != nil {
                // Handle error
                return;
            }
            
            var url=Constants.serviceurl2
            url.append("/happiness/monthly?userIdToken=\(idToken!)")
            print(url)
            var obj = httpRequestController();
            
            
            var response =  obj.request(url: url,type: "Get", completion: { (data )-> Void in
                
                
                
                self.dollars1 = Parser.parseeHappiness(data: data)
                var increment = 0
                for s in self.dollars1
                {
                    self.dollars[increment] = Double(s.happinessValue * 2 + 1)
                    increment += 1
                }
                
                DispatchQueue.main.async(execute: {
                    self.setChartData(months: self.months,values: self.dollars)
                })

                
                
                
            })
        }
        
            
        }
    
    
    func handleSwipes(sender:UISwipeGestureRecognizer) {
        if (sender.direction == .left) {
            tabBarController?.selectedIndex += 1
            
        }
       else if (sender.direction == .right) {
            tabBarController?.selectedIndex -= 1
            
        }
        
    
    
    }
    
}
