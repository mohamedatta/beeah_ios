//
//  UserProfileViewController.swift
//  Beeah
//
//  Created by cloudymac on 6/14/17.
//  Copyright © 2017 cloudymac. All rights reserved.
//

import UIKit
import Firebase
class UserProfileViewController: UIViewController {

    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var happiness: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userName2: UILabel!
    @IBOutlet weak var userProfilePic: UIButton!
    var profilePicData: Data? = nil
    var happinessStr: String = "50% happiness"
    override func viewDidLoad() {
        super.viewDidLoad()
        print("wh")
self.title = "Profile Info"
        print(userProfilePic.frame.width)
        print(userProfilePic.frame.height)

        
        userName.text =  Auth.auth().currentUser?.displayName
        userName2.text =  Auth.auth().currentUser?.displayName
    happiness.text = happinessStr
        email.text = Auth.auth().currentUser?.email
    userProfilePic.setImage(UIImage.init(data: profilePicData!), for: .normal)
        
    
    }
    override func viewWillLayoutSubviews() {
        userProfilePic.layer.cornerRadius = userProfilePic.frame.height/2 //50 // CGFloat(Constants.getCornerRadiusOfButton(hight: Float(userProfilePic.bounds.size.height) , width: Float(userProfilePic.bounds.size.width)))
        
        //0.5 * (userProfilePic?.bounds.size.width)!
        userProfilePic.clipsToBounds = true
        userProfilePic.layer.borderWidth = 1;
        userProfilePic.layer.borderColor = UIColor(red: 0.0/255.0, green: 149.0/225.0, blue: 200.0/255.0, alpha: 1.0).cgColor
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
