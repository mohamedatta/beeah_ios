//
//  ButtonCellDelegate.swift
//  Beeah
//
//  Created by cloudymac on 6/15/17.
//  Copyright © 2017 cloudymac. All rights reserved.
//

import Foundation

protocol ButtonCellDelegate {

    
    func ideaCellTapped(cell: ideaTableViewCell , tag: Int)
    func AnnouncementCellTapped(cell: IntAnnouncementTableViewCell , tag: Int)
    func SocialMediaAnnouncementCellTapped(cell: SocialMediaAnnouncmentTableViewCell , tag: Int)



}
