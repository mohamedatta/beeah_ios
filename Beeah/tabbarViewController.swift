//
//  tabbarViewController.swift
//  Beeah
//
//  Created by cloudymac on 6/11/17.
//  Copyright © 2017 cloudymac. All rights reserved.
//

import UIKit

class tabbarViewController: UITabBarController {
var tabloaded = false
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.tabBar.frame = CGRect(x: 50, y:  view.frame.size.height, width: view.frame.size.width, height: view.frame.size.height)
       

        if #available(iOS 10.0, *) {
            self.tabBar.unselectedItemTintColor = UIColor.white
        } else {
            // Fallback on earlier versions
        }
        
        if(tabloaded == false){
            
//      self.tabBar.
            UITabBar.appearance().selectionIndicatorImage = getImageWithColorPosition(color: UIColor.init(red: 120/255, green: 190/255, blue: 32/255, alpha: 1) , size: CGSize(width:(self.view.frame.size.width)/5,height: 49), lineSize: CGSize(width:(self.view.frame.size.width)/5, height:2))
tabloaded = true
        }
    
    }
    override func viewDidAppear(_ animated: Bool) {
       //  UITabBar.appearance().selectionIndicatorImage = getImageWithColorPosition(color: UIColor.init(red: 120/255, green: 190/255, blue: 32/255, alpha: 1) , size: CGSize(width:(self.view.frame.size.width)/5,height: 49), lineSize: CGSize(width:(self.view.frame.size.width)/5, height:2))
      self.tabBar.selectionIndicatorImage =  getImageWithColorPosition(color: UIColor.init(red: 120/255, green: 190/255, blue: 32/255, alpha: 1) , size: CGSize(width:(self.view.frame.size.width)/5,height: 49), lineSize: CGSize(width:(self.view.frame.size.width)/5, height:2))
    }
    func getImageWithColorPosition(color: UIColor, size: CGSize, lineSize: CGSize) -> UIImage {
        let rect = CGRect(x:0, y: 0, width: size.width, height: size.height)
        let rectLine = CGRect(x:0, y:size.height-lineSize.height,width: lineSize.width,height: lineSize.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        UIColor.clear.setFill()
        UIRectFill(rect)
        color.setFill()
        UIRectFill(rectLine)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        self.view.frame = CGRect(x: 0, y: 0, width: (self.view.superview?.frame.width)!, height: ((self.view.superview?.frame.height)!+50))
        
        super.viewWillLayoutSubviews()
        
        var tabFrame:CGRect = self.tabBar.frame
        tabFrame.origin.y =  self.view.frame.origin.y;
        self.tabBar.frame = tabFrame
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
