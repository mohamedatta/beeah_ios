//
//  ideaTableViewCell.swift
//  Beeah
//
//  Created by cloudymac on 6/11/17.
//  Copyright © 2017 cloudymac. All rights reserved.
//

import UIKit

class ideaTableViewCell: UITableViewCell {
 var buttonDelegate: ButtonCellDelegate?
    @IBOutlet weak var rate5: UIButton!
    @IBOutlet weak var rate4: UIButton!
    @IBOutlet weak var rate3: UIButton!
    @IBOutlet weak var rate2: UIButton!
    @IBOutlet weak var rate1: UIButton!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var ncomments: UILabel!
    @IBOutlet weak var nrating: UILabel!
    @IBOutlet weak var content: UILabel!
    @IBOutlet weak var leftIcon: UIButton!
    @IBOutlet weak var title: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var showMore: UIButton!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
    }
    @IBAction func addCommentBtn(_ sender: Any) {
        if let delegate = buttonDelegate {
            delegate.ideaCellTapped(cell: self , tag: 0 )
        }
        
    }
    @IBAction func moreBtn(_ sender: Any) {
    
        if let delegate = buttonDelegate {
            delegate.ideaCellTapped(cell: self ,tag: 1)
        }
        
    }
    @IBAction func titleBtn(_ sender: Any) {
        if let delegate = buttonDelegate {
            delegate.ideaCellTapped(cell: self ,tag: 2)
        }
    }
    @IBAction func leftIconBtn(_ sender: Any) {
        if let delegate = buttonDelegate {
            delegate.ideaCellTapped(cell: self ,tag: 3)
        }
    }
   
  

}
