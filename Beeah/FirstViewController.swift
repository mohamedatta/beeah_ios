//
//  FirstViewController.swift
//  Beeah
//
//  Created by cloudymac on 6/6/17.
//  Copyright © 2017 cloudymac. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import SDCAlertView
import Toast_Swift

class FirstViewController: UIViewController ,GIDSignInUIDelegate{
    var spinner: UIActivityIndicatorView? = nil;
    var alertVisible = false
    @IBOutlet weak var goSignIn: UIButton!
    var  alert = AlertController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 0, green: 149/255, blue: 200/255, alpha: 1)
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        
        GIDSignIn.sharedInstance().uiDelegate = self

        // GIDSignIn.sharedInstance().signIn()
        print("test")
        if Auth.auth().currentUser != nil {
            // User is signed in.
            // ...
            print("he is signed")
                let VC1 =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainViewController")
                let navController = UINavigationController(rootViewController: VC1) // Creating a navigation controller with VC1 at the root of the navigation stack.
                self.present(navController, animated:true, completion: nil)

            
        
        } else {

            
            
            
            Auth.auth().addStateDidChangeListener { auth, user in
                if let user = user{
                    print(user.email)
                    
                    if((user.email?.contains("@beeah"))!||(user.email?.contains("@cloudy"))!||(user.email?.contains("flyflyerson@gmail.com"))!)
                    {                self.spinner?.stopAnimating()
                        //      alert?.dismiss()
                        self.alert.dismiss(animated: false, completion:{
                            self.alertVisible = false
                            let VC1 =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainViewController")
                            let navController = UINavigationController(rootViewController: VC1) // Creating a navigation controller with VC1 at the root of the navigation stack.
                            self.present(navController, animated:true, completion: nil)
                        })
                        
                    }
                    else
                    {
                        
                        GIDSignIn.sharedInstance().signOut()
                        
                        let firebaseAuth = Auth.auth()
                        do {
                            try firebaseAuth.signOut()
                        } catch let signOutError as NSError {
                            print ("Error signing out: %@", signOutError)
                        }
                        
                        print(self.alertVisible)
                        
                        self.alert.dismiss(animated: false, completion:{
                            self.alertVisible = false
                            // create a new style
                            var style = ToastStyle()
                            
                            // this is just one of many style options
                            style.messageColor = UIColor.white
                            
                            // present the toast with the new style
                            self.view.makeToast("please login with Bee'ah account.", duration: 3.0, position: .center  , style: style)
                            
                            // or perhaps you want to use this style for all toasts going forward?
                            // just set the shared style and there's no need to provide the style again
                            ToastManager.shared.style = style
                            
                            // toggle "tap to dismiss" functionality
                            ToastManager.shared.tapToDismissEnabled = true
                        })
                        print("signed out 2ohhhhhhhh")
                        
                     
                        
                        
                    }
                    
                    
                } else {
                    print("signed out 2ohhhhhhhh")
                    
                    
                }}
            
            
        
        }
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
    public func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!)
{
    
    viewController.dismiss(animated: true, completion: {
 self.showAlertView()
        self.viewDidLoad()
    })
    
        print("how")
        
    }
    
    
    func showAlertView()
    {
        self.alertVisible = true
        print(self.alertVisible)
        spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner?.translatesAutoresizingMaskIntoConstraints = false
        
        
        alert = AlertController(title: "Message", message: "Please wait...")
        alert.contentView.addSubview(spinner!)
        spinner?.startAnimating()
        spinner?.centerXAnchor.constraint(equalTo: (alert.contentView.centerXAnchor)).isActive = true
        spinner?.topAnchor.constraint(equalTo: (alert.contentView.topAnchor)).isActive = true
        spinner?.bottomAnchor.constraint(equalTo: (alert.contentView.bottomAnchor)).isActive = true
        
        self.present(alert, animated: true, completion: nil)
        
    }
    func dismissAlertView()
    {
        self.alertVisible = false
        spinner?.stopAnimating()
        //      alert?.dismiss()
        print("dismissed")
        alert.dismiss(animated: false, completion:nil)
        
    }

    
    @IBAction func signOutBtn(_ sender: Any) {
        print("start sign out")
        GIDSignIn.sharedInstance().signOut()
        
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
    @IBAction func signInBtn(_ sender: Any) {
        
        
        

        
        GIDSignIn.sharedInstance().signIn()
        
        print("clicked")
    //    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
     //   let newViewController = storyBoard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
//        self.present(newViewController, animated: true, completion: nil)
     //   self.navigationController?.pushViewController(newViewController, animated: true)
        
    }
       
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    
    

}
