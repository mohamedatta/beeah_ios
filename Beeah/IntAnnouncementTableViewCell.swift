//
//  IntAnnouncementTableViewCell.swift
//  Beeah
//
//  Created by cloudymac on 6/18/17.
//  Copyright © 2017 cloudymac. All rights reserved.
//

import UIKit

class IntAnnouncementTableViewCell: UITableViewCell {
 var buttonDelegate: ButtonCellDelegate?
    
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var dislikeBtn: UIButton!
    @IBOutlet weak var ndislikes: UILabel!
    @IBOutlet weak var nlikes: UILabel!
    @IBOutlet weak var content: UITextView!
    @IBOutlet weak var moreBtn: UIButton!
    
    var cellindex: IndexPath? = nil
    var expanded = false
    var likeStatus = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        print("how")
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func moreBtnAction(_ sender: Any) {
        
        if let delegate = buttonDelegate {
            delegate.AnnouncementCellTapped(cell: self , tag: 0 )

        }
      

        
    }
    @IBAction func dislikeBtnAction(_ sender: Any) {
        print("dislike")
        if let delegate = buttonDelegate {
            delegate.AnnouncementCellTapped(cell: self , tag: 2 )
        }
 }
    @IBAction func likeBtnAction(_ sender: Any) {
        if let delegate = buttonDelegate {
            delegate.AnnouncementCellTapped(cell: self , tag: 1 )
        }
    
    
    
    }
}
