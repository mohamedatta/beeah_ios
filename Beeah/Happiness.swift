//
//  Happiness.swift
//  Beeah
//
//  Created by Mostafa Shuman on 7/2/17.
//  Copyright © 2017 cloudymac. All rights reserved.
//

import Foundation
import  Firebase



class Happiness {
    var month = Int()
    var happinessValue = Int()
    init(aDict: [String: Int]) {
        self.month = aDict["month"]! as Int
        self.happinessValue = aDict["happinessValue"]! as Int
    }
}
