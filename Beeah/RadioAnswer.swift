//
//  RadioAnswer.swift
//  Beeah
//
//  Created by cloudymac on 6/29/17.
//  Copyright © 2017 cloudymac. All rights reserved.
//

import Foundation

class RadioAnswer: Answer {
    
    var Choice = String()
    
    init(id: String , Choice: String) {
        super.init(id: id)
        self.Choice = Choice
        
    }
    
    
}
