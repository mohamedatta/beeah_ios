//
//  PollsDetailsTableViewCell.swift
//  Beeah
//
//  Created by Mostafa Shuman on 6/26/17.
//  Copyright © 2017 cloudymac. All rights reserved.
//

import UIKit

class PollsDetailsTableViewCell: UITableViewCell {
    @IBOutlet var radBtn:UIImageView!
    @IBOutlet var answersLabel:UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
