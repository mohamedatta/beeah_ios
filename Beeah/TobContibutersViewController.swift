//
//  TobContibutersViewController.swift
//  Beeah
//
//  Created by cloudymac on 6/27/17.
//  Copyright © 2017 cloudymac. All rights reserved.
//

import UIKit
import Firebase

class TobContibutersViewController: UIViewController ,UITableViewDelegate , UITableViewDataSource , TabBarSwitcher{


   public static  var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    
    var currentMonth = 11
    var topContributions: [(Month: Int,Contribution: [Contribution])] = []
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var tableview: UITableView!

    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var month: UILabel!
     var sortedmonths = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        initSwipe(direction: .left)
        initSwipe(direction: .right)
        
        let calendar = Calendar.current
        
        let currentDate =  calendar.dateComponents([.day , .month , .year ], from: Date())
           month.text = TobContibutersViewController.months[currentDate.month! - 1]  // month index in array
        sortedmonths =  currentDate.month!
        if(sortedmonths >= 12  )
        {sortedmonths = 0}
        
        
        requestTopContibutersV2()
        // Do any additional setup after loading the view.
    }
    func handleSwipes(sender:UISwipeGestureRecognizer) {
        if (sender.direction == .left) {
            tabBarController?.selectedIndex += 1
            
        }
        else  if (sender.direction == .right) {
            tabBarController?.selectedIndex -= 1
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        

        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.visibleViewController?.title = "Top Contributors"
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
    
    }
    func enablesBnuttons()
    {
    
        if(currentMonth == 11)
        {
            nextBtn.isEnabled = false
            backBtn.isEnabled = true
        }
        else if(currentMonth == 1)
        {
            backBtn.isEnabled = true
        }
        
        if(currentMonth == 0)
        {
            backBtn.isEnabled = false
           backBtn.isEnabled = true
        }
        else if(currentMonth == 10 )
        {
            
            nextBtn.isEnabled = true
        }
    }
    
   
    func requestTopContibutersV2()
    {
        
        
       
        
        
        let currentUser = Auth.auth().currentUser
        currentUser?.getTokenForcingRefresh(false) {idToken, error in
            if let error = error {
                // Handle error
                return;
            }
            
            var url=Constants.serviceurl2
            url.append("/contributions/list_top_contributers_last_12_monthes?userIdToken=\(idToken!)")
            print(url)
            var obj = httpRequestController();
            
            
            var response =  obj.request(url: url,type: "Get", completion: { (data )-> Void in
                
                
                //  self.tableview.reloadData()
                self.topContributions =  Parser.parseTopContributersV2(data: data)
                
                
                DispatchQueue.main.async {
               self.enablesBnuttons()
                }
                
                

                for i in 0 ... (self.topContributions.count - 1) {

                    if(self.sortedmonths >= 12  )
                    {self.sortedmonths = 0}
                  self.topContributions[i].Month = self.sortedmonths
                    self.sortedmonths = self.sortedmonths + 1
                    
                }
                
                for i in 0 ... (self.topContributions.count - 1) {
                    
             print(self.topContributions[i].Month)
                    
                }
                
                
                print(self.topContributions.count)
                print("data")
                print(data)
                for cont in self.topContributions {
                    
                    
                    for contribution in cont.Contribution
                    {
                        
                        print(contribution.email)
                        /// set profile picture
                        
                        var imageObj = httpRequestController();
                        
                        var imageresponse =  imageObj.request(url: contribution.photoUrl ,type: "Get", completion: { (imagedata )-> Void in
                            
                            contribution.imageData = imagedata

                            DispatchQueue.main.async {
                                self.tableview.reloadData()
                                
                            }
                            
                            
                        })
                        
                    }
                }
            })
            
        }
    }
    

    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if(topContributions.count > currentMonth)
       {return topContributions[currentMonth].Contribution.count}
else
        {return 0;}
    }
        
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:  ContributionTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ContributionTableViewCell", for: indexPath) as! ContributionTableViewCell
 
        cell.name.text = topContributions[currentMonth].Contribution[indexPath.row].name
        cell.contribution.text = "\(topContributions[currentMonth].Contribution[indexPath.row].overAllContribution) Point"
        cell.profilePic.layer.cornerRadius = cell.profilePic.frame.height / 2
        cell.profilePic.clipsToBounds = true
        
        if(topContributions[currentMonth].Contribution[indexPath.row].imageData != nil){
        cell.profilePic.image = UIImage.init(data: topContributions[currentMonth].Contribution[indexPath.row].imageData!)
        }
        return cell
    }
    
    
    
    @IBAction func nextBtnAction(_ sender: Any) {
        
        
        currentMonth += 1
        month.text  = TobContibutersViewController.months[self.topContributions[currentMonth].Month]
        
        if(currentMonth == 11)
        {
            nextBtn.isEnabled = false
        }
        else if(currentMonth == 1)
        {
            backBtn.isEnabled = true
        }
        
        DispatchQueue.main.async {
            self.tableview.reloadData()
            
        }
    }
    @IBAction func backBtnAction(_ sender: Any) {
        currentMonth -= 1
        month.text  = TobContibutersViewController.months[self.topContributions[currentMonth].Month]
        
        if(currentMonth == 0)
        {
            backBtn.isEnabled = false
            
        }
        else if(currentMonth == 10 )
        {
            
            nextBtn.isEnabled = true
        }

        DispatchQueue.main.async {
            self.tableview.reloadData()
            
        }
    }
    

    
}
