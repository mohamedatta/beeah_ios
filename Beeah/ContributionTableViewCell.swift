//
//  ContributionTableViewCell.swift
//  Beeah
//
//  Created by cloudymac on 6/27/17.
//  Copyright © 2017 cloudymac. All rights reserved.
//

import UIKit

class ContributionTableViewCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var contribution: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
