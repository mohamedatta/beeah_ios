//
//  Polls.swift
//  Beeah
//
//  Created by Mostafa Shuman on 6/26/17.
//  Copyright © 2017 cloudymac. All rights reserved.
//

import Foundation
import Foundation
import  Firebase
class Poll{
    var questionText = ""
    var responsesNo = Int()
    var pollType = String()
    var creationDate = Int64()
    var expirationDate = Int64()
    var admin: UserReference? = nil
    var published = Bool()
    var datePublishedFilter = String()
    var pollKey = String()
    var options = [Option]()
    var answers = [Answer]()
    
    var userAnswer: RadioAnswer? = nil
    
    init(aDict: [String: AnyObject]) {
        print(aDict)
        self.questionText = aDict["questionText"] as! String
        self.responsesNo =  aDict["responsesNo"] as! Int
        self.pollType = aDict["pollType"] as! String
        self.creationDate =  aDict["creationDate"] as! Int64
        self.expirationDate =  aDict["expirationDate"] as! Int64
        self.admin =  UserReference(aDict: aDict["admin"] as! [String : AnyObject] )
            // aDict["admin"] as! UserReference
     self.published = aDict["published"] as! Bool
         if  let  dpf =  aDict["datePublishedFilter"] as? String
         {
            self.datePublishedFilter = dpf as! String
            
            }
        self.pollKey =  aDict["pollKey"] as! String
        
        if let optionsdic = aDict["pollOptions"] as? [String: Any] {
            
            for option in optionsdic{
                let quest = option.value as! [String: String]
                
                self.options.append(Option(id: option.key , text: quest["text"]!))
                
            }
            self.options = self.options.sorted (by: {$0.id < $1.id})
            
        }
        
        
        if let pollanswer = aDict["pollAnswer"] as? [String: Any] {
            
            if let choices = pollanswer["choices"] as? [String: Any] {
            
                for choice in choices {
                   let choiceValue = choice.value as! [String: Bool]
                print(Auth.auth().currentUser?.uid)
                    if let usercoice = choiceValue["\((Auth.auth().currentUser?.uid)!)"]
                {
                  userAnswer = RadioAnswer(id: (Auth.auth().currentUser?.uid)!, Choice: choice.key)
                    print(choice.key)
                    
                    }
                    
                    for ch in choiceValue {
                    
                    self.answers.append(RadioAnswer(id: ch.key, Choice: choice.key))
                        
                    }
                    
                }
                
            
            }
            
            
        }
        
    
    }
    
}

