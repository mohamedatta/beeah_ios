//
//  PostingIdeaViewController.swift
//  Beeah
//
//  Created by cloudymac on 6/14/17.
//  Copyright © 2017 cloudymac. All rights reserved.
//

import UIKit
import Firebase

class PostingIdeaViewController: UIViewController , UITextViewDelegate ,UITextFieldDelegate{
    
    @IBOutlet weak var ideaContent: UITextView!
    @IBOutlet weak var ideaTitle: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        let add = UIBarButtonItem(title: "Submit", style: .plain, target: self, action: #selector(addIdea))
        
        add.tintColor = UIColor.white
        //let play = UIBarButtonItem(title: "Play", style: .plain, target: self, action: #selector(addTapped))
        
        self.navigationItem.rightBarButtonItems = [add]
        self.title = "Post Idea"
        
        var borderColor : UIColor = UIColor(red: 0.85, green: 0.85, blue: 0.85, alpha: 1.0)
        ideaContent.layer.borderWidth = 0.5
        ideaContent.layer.borderColor = borderColor.cgColor
        ideaContent.layer.cornerRadius = 5.0
        ideaContent.text = "Message"
        ideaContent.textColor = UIColor.lightGray
        
        ideaContent.delegate = self
        ideaTitle.delegate = self
        
        // Do any additional setup after loading the view.
    }

   
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addIdea(){
        
        if((ideaTitle.text?.characters.count)! < 1)
        {return }
    
        let currentUser = Auth.auth().currentUser
        currentUser?.getTokenForcingRefresh(true) {idToken, error in
            if let error = error {
                // Handle error
                return;
            }
            print("verified")
            print(idToken!)
            
            
            var url = Constants.serviceurl2;
            let allowedCharacterSet = (CharacterSet(charactersIn: "!*'();:@&=+$,/?%#[] \n").inverted)
  
                url.append("/ideas?userIdToken=\(idToken!)&title=\(self.ideaTitle.text!.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet)!)&description=\(self.ideaContent.text!.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet)!)")
            
            print(url)
            var obj = httpRequestController();
            
            
            var response =  obj.request(url: url,type: "Post", completion: { (data )-> Void in
           
            
            
            });
            
self.navigationController?.popViewController(animated: true)
                
            
        
        
    }
    
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.ideaTitle.resignFirstResponder()

        
        return true
    }
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        self.ideaContent.resignFirstResponder()
        
        
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
      print("textview")
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
