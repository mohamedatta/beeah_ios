//
//  IdeaCommentTableViewCell.swift
//  Beeah
//
//  Created by cloudymac on 6/16/17.
//  Copyright © 2017 cloudymac. All rights reserved.
//

import UIKit

class IdeaCommentTableViewCell: UITableViewCell {

    @IBOutlet weak var userpic: UIButton!
    @IBOutlet weak var comment: UITextView!
    @IBOutlet weak var date: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
