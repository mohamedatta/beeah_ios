//
//  SocialMediaTableViewController.swift
//  Beeah
//
//  Created by cloudymac on 6/22/17.
//  Copyright © 2017 cloudymac. All rights reserved.
//

import UIKit
import Firebase
class SocialMediaTableViewController: UITableViewController , ButtonCellDelegate ,TabBarSwitcher{
@IBOutlet var tableview: UITableView!
    var announcments = [SocialMediaAnnouncment]()
    var cellHeights: [IndexPath : CGFloat] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initSwipe(direction: .right)
        initSwipe(direction: .left)
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
 
        tableview.rowHeight = UITableViewAutomaticDimension
         requestAnnouncements()
  
        
    }
    
    func handleSwipes(sender:UISwipeGestureRecognizer) {
        if (sender.direction == .right) {
            tabBarController?.selectedIndex -= 1
        }
       else if (sender.direction == .left) {
            tabBarController?.tabBarController?.selectedIndex += 1
        }
        
    }

    override func viewWillLayoutSubviews() {
        
        
        
        
        super.viewWillLayoutSubviews()
//        self.view.frame = CGRect(x: 0, y: 98 , width: (self.view.superview?.frame.width)!, height: ((self.view.superview?.frame.height)!-150))
        
        
        self.tableview.frame = CGRect(x: self.tableview.frame.minX, y: CGFloat(Constants.tabbarHeight * 2) , width: self.tableview.frame.width, height: self.tableview.frame.height )
        
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
    //    self.navigationController?.visibleViewController?.title = "Social Media"
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
    func requestAnnouncements()
    {
        
        
        let currentUser = Auth.auth().currentUser
        currentUser?.getTokenForcingRefresh(true) {idToken, error in
            if let error = error {
                // Handle error
                return;
            }
            
            var url=Constants.serviceurl2
            url.append("/announcements/social?userTokenId=\(idToken!)")
            print(url)
            var obj = httpRequestController();
            
            
            var response =  obj.request(url: url,type: "Get", completion: { (data )-> Void in
                
                
                
                
               self.announcments = Parser.parseSocialMediaAnnouncements(data: data) as! [SocialMediaAnnouncment]
           
           //     self.options = self.options.sorted (by: {$0.id < $1.id})
              //  yyyy-MM-dd'T'HH:mm:ssZ
                self.announcments   = self.announcments.sorted (by: {$0.sortDate > $1.sortDate} )
                DispatchQueue.main.async(execute: {
                    // Update your UI here
                    self.tableView.reloadData()
                })
                
                
            })
            
            
            
        }
    }
    
    
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return announcments.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier: "SMannouncmentcell", for: indexPath) as! SocialMediaAnnouncmentTableViewCell
        if cell.buttonDelegate == nil {
            cell.buttonDelegate = self
        }
        
        
        var t = announcments[indexPath.row].text
        if(!announcments[indexPath.row].isExpanded){
             cell.moreBtn.setTitle("...More", for: .normal)
            var ind = Constants.getindexOfCharInString(nth: 20, str: t, ch: " ")
            if(ind == -1)
            {
                ind = t.characters.count
            }
            
            
            cell.content.textContainer.maximumNumberOfLines = 2
            
            cell.content.text =  t
            
            
//            cell.content.text =  t.substring(to:t.index( t.startIndex , offsetBy: ind))
        }
        else {
            cell.moreBtn.setTitle("...Less", for: .normal)
            cell.content.textContainer.maximumNumberOfLines = 100
            
            cell.content.text =  t
            
        }
        
        cell.nlikes.text = "\(announcments[indexPath.row].nlikes)"
        
        if(announcments[indexPath.row].providor == "Facebook")
        {
        cell.providerimage.image = UIImage(named: "facebook")
        }
        else if(announcments[indexPath.row].providor == "Twitter")
        {
            cell.providerimage.image = UIImage(named: "twitter")
            
        }
        else if(announcments[indexPath.row].providor == "Instagram")
        {
            cell.providerimage.image = UIImage(named: "instagram")
            
        }
        
        cell.ndislikes.text = "\(announcments[indexPath.row].ndislikes)"
        cell.likeBtn.isEnabled = true
        cell.dislikeBtn.isEnabled = true
        
  
        
        
        
        if(announcments[indexPath.row].userLikeType == "like")
        {
            cell.likeBtn.isEnabled = false
            cell.dislikeBtn.isEnabled = true
            
        }
        else if(announcments[indexPath.row].userLikeType == "dislike" )
        {
            cell.likeBtn.isEnabled = true
            cell.dislikeBtn.isEnabled = false
            
        }
        
        
        
        
        
        cell.cellindex  = indexPath
        
     cell.layoutSubviews()
        return cell
    }

    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func ideaCellTapped(cell: ideaTableViewCell , tag: Int)
    {}
    func AnnouncementCellTapped(cell: IntAnnouncementTableViewCell , tag: Int)
    {}

    func SocialMediaAnnouncementCellTapped(cell: SocialMediaAnnouncmentTableViewCell , tag: Int)
    {
    
        if(tag == 0)// showmore
        {     //   cell.content.text = announcments[(cell.cellindex?.row)!].text
            
          announcments[(cell.cellindex?.row)!].isExpanded = !announcments[(cell.cellindex?.row)!].isExpanded
            self.tableview.reloadRows(at: [cell.cellindex!], with: UITableViewRowAnimation.none)
            tableview.scrollToRow(at: cell.cellindex!, at: .top, animated: true)
            
        }
        else if(tag == 1 )// like button
        {
            cell.likeBtn.isEnabled = false
            requestSocialAnnouncementLike(cell: cell , type: "like", announcmentId: announcments[(cell.cellindex?.row)!].id)
        }
        else if (tag == 2)
        {
            cell.dislikeBtn.isEnabled = false
            requestSocialAnnouncementLike(cell: cell ,type: "dislike", announcmentId: announcments[(cell.cellindex?.row)!].id)
            
        }

        
    }
    
    
    
    
    func requestSocialAnnouncementLike(cell: SocialMediaAnnouncmentTableViewCell , type: String , announcmentId: String)
    {
        
        
        let currentUser = Auth.auth().currentUser
        currentUser?.getTokenForcingRefresh(true) {idToken, error in
            if let error = error {
                // Handle error
                return;
            }
            
            var url=Constants.serviceurl2
            url.append("/announcements/\(announcmentId)/interactions/social?userTokenId=\(idToken!)&announcmentId=\(announcmentId)&type=\(type)")
            
            var obj = httpRequestController();
            print(url)
            
            var response =  obj.request(url: url,type: "Post", completion: { (data )-> Void in
                
                
                if(type == "like")
                {
                    
                    self.announcments[(cell.cellindex?.row)!].nlikes += 1
                    if(self.announcments[(cell.cellindex?.row)!].userLikeType == "dislike")
                    {
                        self.announcments[(cell.cellindex?.row)!].ndislikes -= 1
                        
                    }
                }
                else if(type == "dislike")
                {
                    self.announcments[(cell.cellindex?.row)!].ndislikes += 1
                    
                    if(self.announcments[(cell.cellindex?.row)!].userLikeType == "like")
                    {
                        self.announcments[(cell.cellindex?.row)!].nlikes -= 1
                    }
                    
                    
                    
                }
                
                self.announcments[(cell.cellindex?.row)!].userLikeType = type
                DispatchQueue.main.async {
                    self.tableview.reloadRows(at: [cell.cellindex!], with: UITableViewRowAnimation.none)
                    
                }
                
                
            })
            
            
        }
    }
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat(Constants.tabbarHeight)
    }

    override  func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        return UIView(frame: (self.tableview.tableFooterView?.frame)!)
    }
    
   override  func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeights[indexPath] = cell.frame.size.height
    }
    
   override  func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let height = cellHeights[indexPath] else { return 70.0 }
        return height
    }
    
    
}
