//
//  MainViewController.swift
//  Beeah
//
//  Created by cloudymac on 6/6/17.
//  Copyright © 2017 cloudymac. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import SDCAlertView
import Foundation

class MainViewController: UIViewController {
    var handle: AuthStateDidChangeListenerHandle? = nil
    var qrequested = false
    @IBOutlet weak var happinesslabel: UILabel!
    var alert:AlertController? = nil;
    var spinner: UIActivityIndicatorView? = nil;
     var profilePicData: Data? = nil
    @IBOutlet weak var ideasContainer: UIView!
    @IBOutlet weak var header: UIView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var profilePic: UIButton!
    @IBOutlet weak var segmentcontroll: UISegmentedControl!
    var currentViewController: UIViewController? = nil
    var tabbarViewController: UIViewController? = nil
    var announcementsViewController: UIViewController? = nil
    
    @IBOutlet weak var annoucementsContainer: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Check Connection 
        if(Reachability.isConnectedToNetwork()){
            print("Internet Connection  Available!")
        }
        else
        {
            let connectionAlert = UIAlertController(title: "Internet Connection Error", message: "Oh no!\nnternet Connection not Available!", preferredStyle: .alert)
            let connectionAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            connectionAlert.addAction(connectionAction)
            present(connectionAlert, animated: true, completion: nil)
        }
        
       // let add = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(subMenu))
        let add = UIBarButtonItem(title: "...", style: UIBarButtonItemStyle.done, target: self, action: #selector(subMenu))
        add.tintColor = UIColor.white
        
        let logo = UIBarButtonItem(image: UIImage.init(named: "swirl"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(noAction))
        logo.tintColor = UIColor.white
        
        
        //let play = UIBarButtonItem(title: "Play", style: .plain, target: self, action: #selector(addTapped))
        
        self.navigationItem.rightBarButtonItems = [add]
        self.navigationItem.leftBarButtonItems = [logo]
  
//        self.navigationItem.titleView = UIImageView(image:UIImage.init(named: "swirlwhite") )
        var nav = self.navigationController?.navigationBar
        nav?.barTintColor = UIColor.init(red: 0, green: 149/255, blue: 200/255, alpha: 1)
        
        nav?.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        
        requestUserHappiness()
        

        
       
        
        
       
        
        /// set profile picture
        
        var obj = httpRequestController();
        var response =  obj.request(url: "\(Auth.auth().currentUser!.photoURL!)",type: "Get", completion: { (data )-> Void in
            
            self.profilePicData = data
            Constants.profilePicData = data
            self.profilePic.setImage(UIImage.init(data: data), for: .normal)
          self.profilePic.imageView?.contentMode = UIViewContentMode.scaleAspectFit

        })
        
        ///////////////////
        
        
        // set username
        
        self.usernameLabel.text = Auth.auth().currentUser?.displayName
        
        //////
        
        initializeViewControllers()
   
        self.add(asChildViewController: tabbarViewController!, containers: ideasContainer)
    
        
    
        
    }
    

    
    override func viewWillLayoutSubviews() {
          self.layoutProfilePic()
        let status_height = UIApplication.shared.statusBarFrame.size.height
        print(status_height)
  //  self.ideasContainer.frame = CGRect(x: self.ideasContainer.frame.minX, y: self.ideasContainer.frame.minY, width: self.ideasContainer.frame.width, height:
    //    self.ideasContainer.frame.height - status_height - (self.navigationController?.navigationBar.bounds.height)! )
//        print(self.view.frame.height)
  //      print(self.navigationController?.navigationBar.bounds.height)
        
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.title=""
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if(handle != nil){
            Auth.auth().removeStateDidChangeListener(handle!)}
    }
    
    func requestQuestion()
    {

        
        
        let currentUser = Auth.auth().currentUser
        currentUser?.getTokenForcingRefresh(false) {idToken, error in
            if let error = error {
                // Handle error
                return;
            }
            print("verified")
            print(idToken!)
            var url = "\(Constants.serviceurl2)/beeah/question/istherequestion?userTokenId=\(idToken!)"

            
            var obj = httpRequestController();
          
            print(url)
            var response =  obj.request(url: url,type: "Get", completion: { (data )-> Void in

                print("ok ok")
                print(data)
                
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                if let dictionary = json as? [String: Any] {
                    
                    if let bool = dictionary["question"] as? Bool {
               
                    print(bool)
                        
                        
                        if(bool == true)
                        {
                            let question = dictionary["text"]!
                            let questionid = dictionary["questionid"]!
                            
                            print(question)
                            let questionViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CustomAlertViewController") as UIViewController
                            questionViewController.setValue(question, forKey: "questionText")
                            
                            questionViewController.setValue(questionid, forKey: "questionId")
                            
                            
                            self.spinner?.stopAnimating()
                            //      alert?.dismiss()
                            print("dismissed")
                            self.alert?.dismiss(animated: true, completion:{
                                self.present(questionViewController, animated: false, completion: nil)
                                
                            })
                            

                            
                        }
                        else
                        {self.dismissAlertView()}
                    }
                    else
                    {self.dismissAlertView()}
                }
                else
                {self.dismissAlertView()}
            })
            
            print(response)
        
            
    }
    }
    
    
    func initializeViewControllers()
    {
    
         tabbarViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabbarViewController") as UIViewController
        
        tabbarViewController?.view.frame = CGRect(x: 0, y: 0, width: ideasContainer.frame.width, height: ideasContainer.frame.height)
         
        
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
       
        
        handle  =   Auth.auth().addStateDidChangeListener { auth, user in
            if let user = user {
                print("oh mohamed ")
                
                
                
                // User is signed in. Show home screen
            } else {
                let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FirstViewController") as UIViewController
                // .instantiatViewControllerWithIdentifier() returns AnyObject! this must be downcast to utilize it
                
                // self.present(viewController, animated: false, completion: nil)
                
                self.dismiss(animated: true, completion: nil)
                print("signed out ohhhhhhhh")
                
            }}
        if qrequested == false {
            qrequested = true

            showAlertView()
            requestQuestion()
}
        

    }
    
    private func add(asChildViewController viewController: UIViewController,containers container: UIView) {
        // Add Child View Controller
        addChildViewController(viewController)
        
        // Add Child View as Subview
        //        view.addSubview(viewController.view)
        container.addSubview(viewController.view)
        //          viewController.view.frame = ideasContainer.frame
        // Notify Child View Controller
        
//        viewController.view.frame.
        viewController.didMove(toParentViewController: self)
        
        
    }
   
    func hideContentController(content: UIViewController) {
        content.willMove(toParentViewController: nil)
        content.view.removeFromSuperview()
        content.removeFromParentViewController()
    }
    func subMenu()
    {
        print("rrrr")
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        
        let  signOutButton = UIAlertAction(title: "SignOut!", style: .destructive, handler: { (action) -> Void in
            print("signout button tapped")
            GIDSignIn.sharedInstance().signOut()
            
            let firebaseAuth = Auth.auth()
            do {
                try firebaseAuth.signOut()
            } catch let signOutError as NSError {
                print ("Error signing out: %@", signOutError)
            }
            
        })
        
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            print("Cancel button tapped")
        })
        
        
        alertController.addAction(signOutButton)
        alertController.addAction(cancelButton)
        
        alertController.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem

        self.present(alertController, animated: true, completion: nil)
        

    }
    @IBAction func indexChanged(_ sender: Any) {
        switch segmentcontroll.selectedSegmentIndex
        {
        case 0:
            print ("First Segment Selected");
            print("yy")
            hideContentController(content: currentViewController!)
            currentViewController = tabbarViewController
            add(asChildViewController: currentViewController!, containers: ideasContainer)
            
        case 1:
            hideContentController(content: currentViewController!)
currentViewController = announcementsViewController
            currentViewController?.view.frame = CGRect(x: 0, y: 0, width: ideasContainer.frame.width, height: ideasContainer.frame.height)

            add(asChildViewController: currentViewController!, containers: ideasContainer)
            print("Second Segment Selected");
        default:
            break
        }
        
    }
    
    
    
    func showAlertView()
    {
         spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner?.translatesAutoresizingMaskIntoConstraints = false
       
        
         alert = AlertController(title: "Message", message: "Please wait...")
        alert?.contentView.addSubview(spinner!)
         spinner?.startAnimating()
        spinner?.centerXAnchor.constraint(equalTo: (alert?.contentView.centerXAnchor)!).isActive = true
        spinner?.topAnchor.constraint(equalTo: (alert?.contentView.topAnchor)!).isActive = true
        spinner?.bottomAnchor.constraint(equalTo: (alert?.contentView.bottomAnchor)!).isActive = true
        
        self.present(alert!, animated: true, completion: nil)

    }
    func dismissAlertView()
    {
    
        spinner?.stopAnimating()
  //      alert?.dismiss()
        print("dismissed")
        alert?.dismiss(animated: false, completion:nil)
    
    }
    
    func requestCompleted( response:String)
    {
        
        
        var response = response
    print("mohamed "+response)
    
    
    }
    
    
    
    
    func layoutProfilePic(){
       
        
        
//        profilePic.frame = CGRect(x: profilePic.frame.origin.x, y: profilePic.frame.origin.y, width: profilePic.frame.width, height: profilePic.frame.width)
        profilePic.layer.cornerRadius = profilePic.frame.width/2 //profilePic.bounds.width / 2 //CGFloat(Constants.getCornerRadiusOfButton(hight: Float(profilePic.bounds.size.height) , width: Float(profilePic.bounds.size.width)))
        //self.profilePic.layer.borderWidth = 3.0
       print("how")
        print(profilePic.bounds.width)
        print(profilePic.bounds.height)
        profilePic.clipsToBounds = true
        
        profilePic.layer.borderWidth = 1;
        profilePic.layer.borderColor = UIColor(red: 0.0/255.0, green: 149.0/225.0, blue: 200.0/255.0, alpha: 1.0).cgColor
        
    //    profilePic.layer.cornerRadius = profilePic.layer.bounds.width / 2; // this value vary as per your desire
//profilePic.layer.cornerRadius = 40.6
        //        profilePic.layer.borderColor = UIColor.black.cgColor
    }
    
    
    @IBAction func profileBtn(_ sender: Any) {
        
        let VC1 =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "UserProfileViewController")
     VC1.setValue(profilePicData, forKey: "profilePicData")
        VC1.setValue(happinesslabel.text, forKey: "happinessStr")
        self.navigationController?.pushViewController(VC1, animated: true)
        
        
    }
    
    func requestUserHappiness()
    {
        
        
        let currentUser = Auth.auth().currentUser
        currentUser?.getTokenForcingRefresh(true) {idToken, error in
            if let error = error {
                // Handle error
                return;
            }
            
            print("verified")
            print(idToken!)
            var url=Constants.serviceurl2
            url.append("/happiness?userIdToken=\(idToken!)")
            
            var obj = httpRequestController();

            print("happiness")
print(url)
            
            var response =  obj.request(url: url,type: "Get", completion: { (data )-> Void in
                
print(data)
                
                Constants.happinessVal = Parser.parseeUserHappiness(data: data)
                Constants.happinessStr = "\(Constants.happinessVal) % happiness"
               DispatchQueue.main.async(execute:
                {
                    self.happinesslabel.text = Constants.happinessStr

               
               }
                
                )
                
            })
            
            print(response)
            
            
        }
    }
    
    func noAction(){
    }
    
    
    
    
}
