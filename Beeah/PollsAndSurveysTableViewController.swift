//
//  PollsAndSurveysTableViewController.swift
//  Beeah
//
//  Created by Mostafa Shuman on 6/26/17.
//  Copyright © 2017 cloudymac. All rights reserved.
//

import UIKit
import Firebase

class PollsAndSurveysTableViewController: UITableViewController ,TabBarSwitcher{

    var polls = [Poll]()

    override func viewDidLoad() {
        super.viewDidLoad()
        initSwipe(direction: .left)
        initSwipe(direction: .right)
        
        requestPolls()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor.white
    }
    func handleSwipes(sender:UISwipeGestureRecognizer) {
        if (sender.direction == .left) {
            tabBarController?.selectedIndex += 1
        }
        else if (sender.direction == .right) {
            tabBarController?.selectedIndex -= 1
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.visibleViewController?.title = "Poll & Surveys"

      //  tableView.reloadData()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
    
    }
    func requestPolls()
    {
        
        
        let currentUser = Auth.auth().currentUser
        currentUser?.getTokenForcingRefresh(true) {idToken, error in
            if error != nil {
                // Handle error
                return;
            }
            
            var url=Constants.serviceurl2
            url.append("/polls?userIdToken=\(idToken!)")
            print(url)
            var obj = httpRequestController();
            
            
            var response =  obj.request(url: url,type: "Get", completion: { (data )-> Void in
                
                
                
                self.polls = Parser.parsPolls(data: data)
                
                
                DispatchQueue.main.async(execute: {
                    // Update your UI here
                    self.tableView.reloadData()
                })
                
                
                
            })
            
            
            
        }
    }
    

    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return polls.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PllsAndSurveysTableViewCell
        cell.questionTitle.text = polls[indexPath.row].questionText
        cell.responded.text = String(polls[indexPath.row].responsesNo)
        cell.thumbnail.layer.cornerRadius = cell.thumbnail.frame.height/2
     
        cell.thumbnail.clipsToBounds = true
        
//        cell.thumbnail.image = UIImage(named: polls[indexPath.row].lock ? "1.png" : "2.png")
//    cell.thumbnail.image = UIImage(named: "1")
        
        
        return cell
        
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PollsDetails"{
            if let indexPath = tableView.indexPathForSelectedRow{
                let  destination = segue.destination as! PollsDetailsViewController
                destination.polls = polls[indexPath.row]
                destination.hidesBottomBarWhenPushed = true
            }
        }
    }
}
