//
//  PollsDetailsViewController.swift
//  Beeah
//
//  Created by Mostafa Shuman on 6/26/17.
//  Copyright © 2017 cloudymac. All rights reserved.
//

import UIKit
import Firebase
class PollsDetailsViewController: UIViewController ,UITableViewDataSource,UITableViewDelegate ,TabBarSwitcher{
    
    @IBOutlet var tableView : UITableView!
    @IBOutlet var hidenView:UIView!
    @IBOutlet var QusLabel:UILabel!
    @IBOutlet var submitBtn:UIButton!
    
    var polls:Poll!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.allowsMultipleSelection = false
        QusLabel.text = polls.questionText
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.isScrollEnabled = false
        submitBtn.isUserInteractionEnabled = false
        submitBtn.backgroundColor = UIColor.lightGray
        self.navigationController?.visibleViewController?.title = "Poll Details"

        
        // isQuestionAnswered()
        
        for i in polls.options{
        print(i.text)
        }
        
        hidenView.isHidden = true
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func handleSwipes(sender:UISwipeGestureRecognizer) {
        if (sender.direction == .left) {
            tabBarController?.selectedIndex += 1
            
        }
    }

    override func viewDidAppear(_ animated: Bool) {
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return polls.options.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PollsDetailsTableViewCell
        cell.answersLabel.text = polls.options[indexPath.row].text
        cell.radBtn.image = cell.isSelected ? UIImage(named: "radiochecked") : UIImage(named: "radiounchecked")
if(polls.userAnswer?.Choice == polls.options[indexPath.row].id)
{
    hidenView.isHidden = false
    cell.radBtn.image = UIImage(named: "radiochecked")

    self.submitBtn.isEnabled = false
        }
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell=tableView.cellForRow(at: indexPath as IndexPath)as? PollsDetailsTableViewCell
        cell?.radBtn.image = UIImage(named: "radiochecked")
        cell?.selectionStyle = .none
        activeSubmitBtn()
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell=tableView.cellForRow(at: indexPath as IndexPath)as? PollsDetailsTableViewCell
        cell?.radBtn.image = UIImage(named: "radiounchecked")
        
    }
    
    
 /*   func isQuestionAnswered(){
        if polls.lock{
            //            myAnswer()
            hidenView.isHidden = false
            hidenView.backgroundColor = UIColor(red: 127.0/255.0, green: 127.0/255.0, blue: 127.0/255.0, alpha: 0.0)
            submitBtn.backgroundColor = UIColor.lightGray
            submitBtn.isEnabled = false
            
        }else{
            hidenView.isHidden = true
            submitBtn.isEnabled = true
        }
    }
    
    func myAnswer(){
        let indexPath = IndexPath(row: polls.answer, section: 0)
        print(polls.answer)
        let cell = self.tableView.cellForRow(at: indexPath as IndexPath)as? PollsDetailsTableViewCell
        cell?.radBtn.image = UIImage(named: "radiochecked")
    }
   */
    func activeSubmitBtn(){
        submitBtn.isUserInteractionEnabled = true
        submitBtn.backgroundColor = UIColor(red: 18.0/255.0, green: 152.0/255.0, blue: 185.0/255.0, alpha: 1.0)
    }
    
    @IBAction func submited(sender:UIButton){
        polls.userAnswer = RadioAnswer(id: (Auth.auth().currentUser?.uid)!, Choice: polls.options[(self.tableView.indexPathForSelectedRow?.row)!].id)
 
        
        var databaseRef: DatabaseReference!
        var answerref: DatabaseReference!
        
        databaseRef = Database.database().reference()
          answerref = databaseRef.child("polls").child("\(polls.pollKey)").child("pollAnswer").child("choices").child("\((polls.userAnswer?.Choice)!)").child("\((Auth.auth().currentUser?.uid)!)")
        //        requestIdeas()
        print(answerref.ref.url)
        
        answerref.setValue(true)
        
        self.tableView.reloadData()
        submitBtn.backgroundColor = UIColor.lightGray
        
    }


}
