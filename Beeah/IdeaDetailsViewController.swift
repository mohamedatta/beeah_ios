//
//  IdeaDetailsViewController.swift
//  Beeah
//
//  Created by cloudymac on 6/15/17.
//  Copyright © 2017 cloudymac. All rights reserved.
//

import UIKit
import Firebase
class IdeaDetailsViewController: UIViewController ,UITextViewDelegate , UITableViewDelegate , UITableViewDataSource{
   public  var idea: Idea? = nil;
    var oldframe: CGRect? = nil
    var scrolloldframe: CGRect? = nil
    
    var comments = [Comment]()
    var str: String = ""
    @IBOutlet weak var tableview: UITableView!
  var profilePicData: Data? = nil
    var happinessStr: String = "50% happiness"

    @IBOutlet weak var commentStackView: UIStackView!
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var rateBtn3: UIButton!
    @IBOutlet weak var rateBtn4: UIButton!
    @IBOutlet weak var rateBtn1: UIButton!
    @IBOutlet weak var happiness: UILabel!
    
    @IBOutlet weak var rateBtn5: UIButton!
    @IBOutlet weak var rateBtn2: UIButton!
    @IBOutlet weak var addCommentBtn: UIButton!
    @IBOutlet weak var newComment: UITextView!
   
    @IBOutlet weak var ideaContent: UITextView!
    @IBOutlet weak var ideaTitle: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var profilePic: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let user = Auth.auth().currentUser
        
        profilePicData = Constants.profilePicData
        happinessStr = Constants.happinessStr
   happiness.text = happinessStr
        userName.text = user?.displayName
        ideaTitle.text = idea?.title
        ideaContent.text = idea?.descriptions
    
        
        if(profilePicData != nil){
        profilePic.image = UIImage.init(data: profilePicData!)
        }
        print("ideasize")
        print(profilePic.frame.width)
        print(profilePic.frame.height)

        
            self.title = "Idea Details"
        
        tableview.estimatedRowHeight = 50
        tableview.rowHeight = UITableViewAutomaticDimension

        
        
        var borderColor : UIColor = UIColor(red: 0, green: 149/255, blue: 200/255, alpha: 1.0)
        newComment.layer.borderWidth = 0.5
        newComment.layer.borderColor = borderColor.cgColor
        newComment.layer.cornerRadius = 5.0
        newComment.text = "Type"
        newComment.textColor = UIColor.lightGray
        
        newComment.delegate = self
        
       
     //   modifyRate()
        listenToNewRate()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        
        
        
        
        // get comments from firebase 
        
        var databaseRef: DatabaseReference!
        var commentsRef: DatabaseReference!
        
        databaseRef = Database.database().reference()
     print(idea?.id)
        commentsRef = databaseRef.child("comments").child((idea?.id)!)
  
        commentsRef.observe(.childAdded, with: { (snapshot) -> Void in
            var comment:Comment = Comment(Dict: snapshot.value! as! [String : AnyObject])
            self.comments.append(comment)
            print(snapshot.value)
            self.tableview.reloadData()
            
       
        })
        
        


        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    override func viewWillLayoutSubviews() {
   //   textViewDidChange(textView: ideaContent)
        profilePic.layer.cornerRadius = profilePic.frame.width/2 
        profilePic.clipsToBounds = true
        profilePic.layer.borderWidth = 1;
        profilePic.layer.borderColor = UIColor(red: 0.0/255.0, green: 149.0/225.0, blue: 200.0/255.0, alpha: 1.0).cgColor

        self.newComment.setContentOffset(CGPoint.zero, animated: false)
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
     func modifyRate() {
        
           reImageRates(btn: 0)
        
        for rate in (idea?.usersRate)!{
        
        if(rate.userId == Auth.auth().currentUser?.uid)
        {

            reImageRates(btn: Int(rate.userRate))
          break
            }
        
        }
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
 */
    @IBAction func ideaCommentBtnAction(_ sender: Any) {
        
        if((newComment.text?.characters.count)! < 1)
        {return }
        
        var commenttext = newComment.text
        let currentUser = Auth.auth().currentUser
        currentUser?.getTokenForcingRefresh(true) {idToken, error in
            if let error = error {
                // Handle error
                return;
            }
            
            var url = Constants.serviceurl2;
            let allowedCharacterSet = (CharacterSet(charactersIn: "!*'();:@&=+$,/?%#[] \n").inverted)
            
            url.append("/ideas/\((self.idea?.id.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet))!)/comments?userIdToken=\(idToken!)&userComment=\(commenttext!.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet)!)&ideaKey=\((self.idea?.id.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet))!)")
            
            var obj = httpRequestController();
            
            var response =  obj.request(url: url,type: "Post", completion: { (data )-> Void in
                
                
            });
            
            
            
            
        }
        newComment.text = "Type"
        newComment.textColor = UIColor.lightGray
        
        
    }
 @IBAction func rate5Action(_ sender: Any) {
    showAlertViewForRate(btn: 5)
    
    
    }
 
    @IBAction func rate4Action(_ sender: Any) {
        showAlertViewForRate(btn: 4)

    }
    @IBAction func rate3Action(_ sender: Any) {
        showAlertViewForRate(btn: 3)
    }
    @IBAction func rate2Action(_ sender: Any) {
        showAlertViewForRate(btn: 2)
    }

    @IBAction func rate1Action(_ sender: Any) {
        showAlertViewForRate(btn: 1)
        
    }
    
    
    func reImageRates(btn: Int)
    {
        if(btn >= 1)
        { rateBtn1.setImage( UIImage.init(named: "ideas/filledstar"), for:.normal)
        }
        else
        { rateBtn1.setImage( UIImage.init(named: "ideas/emptystar"), for:.normal)
        }
        if(btn >= 2)
        { rateBtn2.setImage( UIImage.init(named: "ideas/filledstar"), for:.normal)
        }
        else
        { rateBtn2.setImage( UIImage.init(named: "ideas/emptystar"), for:.normal)
        }
        if(btn >= 3)
        { rateBtn3.setImage( UIImage.init(named: "ideas/filledstar"), for:.normal)
        }
        else
        { rateBtn3.setImage( UIImage.init(named: "ideas/emptystar"), for:.normal)
        }
        if(btn >= 4)
        { rateBtn4.setImage( UIImage.init(named: "ideas/filledstar"), for:.normal)
        }
        else
        { rateBtn4.setImage( UIImage.init(named: "ideas/emptystar"), for:.normal)
        }
        if(btn >= 5)
        { rateBtn5.setImage( UIImage.init(named: "ideas/filledstar"), for:.normal)
        }
        else
        { rateBtn5.setImage( UIImage.init(named: "ideas/emptystar"), for:.normal)
        }
        
      
        
        
    }
    
    
    
    
    
    func showAlertViewForRate(btn: Int)
    {
        var uiAlert = UIAlertController(title: "Message", message: "Sure To Rate The Idea .", preferredStyle: UIAlertControllerStyle.alert)
     self.present(uiAlert, animated: true, completion: nil)

        uiAlert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler:
            { (action: UIAlertAction! ) in
                
                
                self.reImageRates(btn: btn)
                self.requestRateIdea(rate: btn)
                
                
                
                
                
                
        }
        ))
            
        uiAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (action: UIAlertAction!) in
            print("cancel")
        }))

        
    }
    
    
    func requestRateIdea(rate: Int)
    {
    
        
        let currentUser = Auth.auth().currentUser
        currentUser?.getTokenForcingRefresh(true) {idToken, error in
            if let error = error {
                // Handle error
                return;
            }
            
            print("verified")
            print(idToken!)
            var url=Constants.serviceurl2
            url.append("/ideas/\((self.idea?.id)!)/rate_idea?userIdToken=\(idToken!)&rate=\(rate)")
            print(url)
            var obj = httpRequestController();
            
            
            var response =  obj.request(url: url,type: "PUT", completion: { (data )-> Void in
                
                print("ok")
                print(data)
                
                
            })
            
            print(response)
            
            
        }
        
    }
    
    
    func listenToNewRate(){
        var databaseRef: DatabaseReference!
        var rateRef: DatabaseReference!
        
        databaseRef = Database.database().reference()
        if(idea?.id == "")
        {idea?.id = "0" // any non empty string
        }
        rateRef = databaseRef.child("ideas").child("\((idea?.id)!)").child("usersRate").child("\((Auth.auth().currentUser?.uid)!)")
        
        rateRef.observeSingleEvent(of: .value , with: {(snapshot) in

            
                     if (!(snapshot.value! is NSNull))
                     {
                self.reImageRates(btn:snapshot.value!  as! Int - 1)
            }
                     else{
            self.reImageRates(btn: 0 )
            }

            
        }   ){
            (error) in
         print("error")
        
        }
        
        
    }
    
    
    
   // MARK: - Table view data source
    
     func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return comments.count
    }
    
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       
        let cell:IdeaCommentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "commentcell", for: indexPath) as! IdeaCommentTableViewCell
        
        cell.comment.text = comments[indexPath.row].comment
        cell.date.text = "\(Constants.getTimeAgo(timeinterval: comments[indexPath.row].sortdate , objDate: comments[indexPath.row].date ,format: "dd-MM-yyyy kk:mm:ss a")) "
        cell.comment.isScrollEnabled = false
    /*
        let contentSize = cell.comment.sizeThatFits(cell.comment.bounds.size)
        
        var frame = cell.comment.frame
        frame.size.height = contentSize.height
        cell.comment.frame = frame
        */
        
        return cell
    }
    
    
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
    }
    
    
    
    func cellTapped(cell: ideaTableViewCell ,tag: Int) {
        
        let VC1 =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "IdeaDetailsViewController") as! IdeaDetailsViewController
        //    VC1.setValue(ideas[tableView.indexPath(for: cell)!.row] as! Idea , forKey:"selectedidea")
        
        self.navigationController?.pushViewController(VC1, animated: true)
        
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
      
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dissmissKeyboard))
        view.addGestureRecognizer(tap)
        if textView.textColor == UIColor.lightGray {
            textView.text = ""
            textView.textColor = UIColor.black
        }
        
        
    }
    
    
    func dissmissKeyboard() {
        newComment.endEditing(true)
        
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
       // scroll.setContentOffset(CGPoint(x: 0,y: 0), animated: true)
        commentStackView.frame = oldframe!
       // scroll.setContentOffset(      CGPoint(x: 0 , y: 0 ), animated: true)
        scroll.frame = scrolloldframe!
        
        
    }
    
    func textViewDidChange(_ textView: UITextView) {
        
        if textView.contentSize.height > textView.frame.size.height {
            
            let fixedWidth = textView.frame.size.width
            textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            
            var newFrame = textView.frame
            let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            
            
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            
          //  textView.frame = newFrame;
        }

    }
    
    func keyboardWillShow(_ notification:Notification)
    {
        let userInfo:NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
        let keyboardFrame:NSValue = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
       let keyboardHeight = keyboardRectangle.height
        
//        commentStackView.bounds.minY = setContentOffset(CGPoint(x: 0,y: keyboardHeight), animated: true)
        
       var frame = commentStackView.frame
 oldframe = commentStackView.frame
        

        frame = CGRect(x: frame.minX, y: self.view.frame.height - keyboardHeight - frame.height - 5 , width: frame.width, height: frame.height)
   
        
        //   scroll.setContentOffset(      CGPoint(x: 0 , y: self.scroll.contentSize.height - (self.view.frame.height -  tableview.frame.minY) ), animated: true)
        
        var scrllframe = scroll.frame
        scrolloldframe = scroll.frame
        
        print("test")
        print(-keyboardHeight )
        
        scrllframe = CGRect(x: scrllframe.minX, y:-keyboardHeight+45  , width: scrllframe.width, height: scrllframe.height)
        
        scroll.frame = scrllframe

        
        commentStackView.frame = frame

        
       
        
    }
    
    
/*    func textViewDidChange(textView: UITextView){
        print("relayout text view")
        let fixedWidth = textView.frame.size.width
        textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        var newFrame = textView.frame
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        textView.frame = newFrame;
        
    }
  */
    
    
}
