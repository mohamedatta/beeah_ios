//
//  IdeasTableViewController.swift
//  Beeah
//
//  Created by cloudymac on 6/7/17.
//  Copyright © 2017 cloudymac. All rights reserved.
//

import UIKit
import Firebase
class IdeasTableViewController: UITableViewController ,ButtonCellDelegate,TabBarSwitcher{
var ideas = [Idea]()
    var button: UIButton? = nil
    @IBOutlet var IdeasTable: UITableView!
      var profilePicData: Data? = nil
    var happinessStr: String = "50% happiness"

    override func viewDidLoad() {
        super.viewDidLoad()
         initSwipe(direction: .left)
// add post button

        button = UIButton(frame: CGRect(x: self.view.frame.width - 70 , y: self.view.frame.height-70, width: 60, height: 60))
    
        button?.backgroundColor = UIColor.init(red: 120/255, green: 190/255, blue: 32/255, alpha: 1)
        var image = UIImage.init(named: "ideas/addideaicon")
        
     // image = CustomAlertViewController.resizeImage(image: image!, withSize: CGSize(width: (button?.frame.width)!*2/3, height: (button?.frame.height)!*2/3))
        
        button?.setImage(image, for: .normal)
        button?.layer.cornerRadius = 0.5 * (button?.bounds.size.width)!
        button?.clipsToBounds = true
        button?.addTarget(self, action: #selector(ratingButtonTapped), for: .touchUpInside)
            // to get data from api
        
//        var idea = Idea()
//        idea.descriptions = "test"
 //       idea.title = "test"
  //      idea.date = "test"
        
  //      ideas.append(idea)
        
        
        
        
        
        
        //
 
        requestIdeas()
        
 
 
    }
    
    
    func upplyRealtTime(afterTime: Int64)
    {
        
        // to get data from firebase directly
        
        
        print(afterTime)
        var databaseRef: DatabaseReference!
        var ideasRef: DatabaseReference!
        
        databaseRef = Database.database().reference()
        let  ideasquery = databaseRef.child("ideas").queryOrdered(byChild: "sortDate").queryStarting(atValue: afterTime) as! DatabaseQuery
        //        requestIdeas()
        
        
        ideasquery.observe(.childAdded, with: { (snapshot) -> Void in
            var idea:Idea = Idea(aDict: snapshot.value! as! [String : AnyObject])
            idea.id = snapshot.key
            var commentsRef = databaseRef.child("comments").child("\(idea.id)")
            commentsRef.observeSingleEvent(of: .value, with: {(snapshot ) -> Void in
                
                idea.ncomments = Int(snapshot.childrenCount)
                self.ideas.insert(idea, at: 0)
          print(idea.sortDate)
                self.tableView.reloadData()
                
            })
            self.tableView.reloadData()
        })

    
    }
    
    func handleSwipes(sender:UISwipeGestureRecognizer) {
        if (sender.direction == .left) {
            tabBarController?.selectedIndex += 1

        }
    }

    
    func ratingButtonTapped() {
        print("Button pressed")
        
        
        let VC1 =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PostingIdeaViewController")
          self.navigationController?.pushViewController(VC1, animated: true)
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addIdeaButton()
        self.navigationController?.visibleViewController?.title = "Ideas & Suggestions"
        
        
    
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        button?.removeFromSuperview()
        
    }
    
    
    func addIdeaButton(){
    
    
        
self.navigationController?.view.addSubview(button!)

    
    }
    override func viewWillLayoutSubviews() {
    
            super.viewWillLayoutSubviews()
            self.view.frame = CGRect(x: 0, y: 10 , width: (self.view.frame.width), height: ((self.view.frame.height)))
            
            
            
            
            
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell:ideaTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ideacell") as! ideaTableViewCell
        
        return cell
    }
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        return 0
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return ideas.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
   
        
        var index = indexPath.row;
        
        let cell:ideaTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ideacell", for: indexPath) as! ideaTableViewCell

        
        
        cell.showMore.isHidden = false
        // Configure the cell...
        
        cell.rate1.setImage( UIImage.init(named: "ideas/emptystar"), for:.normal)
        cell.rate2.setImage( UIImage.init(named: "ideas/emptystar"), for:.normal)
        cell.rate3.setImage( UIImage.init(named: "ideas/emptystar"), for:.normal)
        cell.rate4.setImage( UIImage.init(named: "ideas/emptystar"), for:.normal)
        cell.rate5.setImage( UIImage.init(named: "ideas/emptystar"), for:.normal)
        
        
        
        cell.title.setTitle( ideas[index].title , for: .normal)
        cell.content.text = ideas[index].descriptions
        cell.nrating.text="\(ideas[index].usersRate.count) Rating"
cell.ncomments.text=" \(ideas[index].comments.count) Added To Idea"
        cell.date.text =  Constants.getTimeAgo(timeinterval: ideas[index].sortDate , objDate: ideas[index].date ,format: "dd-MM-yyyy kk:mm:ss a")
      //Constants.getDateTimeShortCut(timeInterval: ideas[index].sortDate)
        if cell.buttonDelegate == nil {
            cell.buttonDelegate = self
        }
        
     if(!isTruncated(label: cell.content))
     {cell.showMore.isHidden = true
        
        }
        
        // configure rates 
        if(ideas[index].overAllRate >= 1){
            cell.rate1.setImage( UIImage.init(named: "ideas/filledstar"), for:.normal)
        }
        if(ideas[index].overAllRate >= 2){
            cell.rate2.setImage( UIImage.init(named: "ideas/filledstar"), for:.normal)
        }
        if(ideas[index].overAllRate >= 3){
            cell.rate3.setImage( UIImage.init(named: "ideas/filledstar"), for:.normal)
        }
        if(ideas[index].overAllRate >= 4){
            cell.rate4.setImage( UIImage.init(named: "ideas/filledstar"), for:.normal)
        }
        if(ideas[index].overAllRate >= 5){
            cell.rate5.setImage( UIImage.init(named: "ideas/filledstar"), for:.normal)
        }
        
        
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    func requestIdeas()
    {
        
        let currentUser = Auth.auth().currentUser
        currentUser?.getTokenForcingRefresh(true) {idToken, error in
            if let error = error {
                // Handle error
                return;
            }
             var url = "\(Constants.serviceurl2)/ideas?userIdToken=\(idToken!)"
            
            var obj = httpRequestController();
            
            print(url)
            var response =  obj.request(url: url,type: "Get", completion: { (data )-> Void in
        
              self.ideas = Parser.parseIdeas(data: data)
                self.ideas =  self.ideas.sorted(by: {$0.sortDate > $1.sortDate})
              
                DispatchQueue.main.async(execute: {
                self.tableView.reloadData()
               
                 })
                // add firebase real time
                if(self.ideas.count>0){
             self.upplyRealtTime(afterTime: self.ideas[0].sortDate + 1)
                }
                
            })
        }

    
    
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
    }
    
    
      
    func ideaCellTapped(cell: ideaTableViewCell ,tag: Int) {
      print(tableView.indexPath(for: cell)!.row)
   
        let VC1 =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "IdeaDetailsViewController") as! IdeaDetailsViewController
   //    VC1.setValue(ideas[tableView.indexPath(for: cell)!.row] as! Idea , forKey:"selectedidea")
     VC1.idea = ideas[tableView.indexPath(for: cell)!.row] as! Idea
        self.navigationController?.pushViewController(VC1, animated: true)
        
        
    }
    
    func AnnouncementCellTapped(cell: IntAnnouncementTableViewCell, tag: Int) {
        
    }
    
    
    func isTruncated(label: UILabel) -> Bool {
            
            if let string = label.text {
                let size: CGSize = (string as NSString).boundingRect(
                    with: CGSize(width: label.frame.size.width, height: CGFloat.greatestFiniteMagnitude),
                    options: NSStringDrawingOptions.usesLineFragmentOrigin,
                    attributes: [NSFontAttributeName: label.font],
                    context: nil).size
             //   print(string)
           //     print(label.bounds.size.height)
             //   print(size.height)
                return (size.height > label.bounds.size.height)
            }
            
            return false
        }
        
    
    func SocialMediaAnnouncementCellTapped(cell: SocialMediaAnnouncmentTableViewCell , tag: Int)
    {}
    
}
