//
//  AnnouncmentsTabBarController.swift
//  Beeah
//
//  Created by cloudymac on 6/11/17.
//  Copyright © 2017 cloudymac. All rights reserved.
//

import UIKit

class AnnouncmentsTabBarController: UITabBarController {
    var parenttabbarhight : CGFloat = 0.0;
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 10.0, *) {
            self.tabBar.unselectedItemTintColor = UIColor.init(red: 0, green: 149/255, blue: 200/255, alpha: 1)
        } else {
            // Fallback on earlier versions
        }
        
        UITabBar.appearance().selectionIndicatorImage = getImageWithColorPosition(color: UIColor.init(red: 120/255, green: 190/255, blue: 32/255, alpha: 1) , size: CGSize(width:(self.view.frame.size.width)/2,height: 49), lineSize: CGSize(width:(self.view.frame.size.width)/2, height:2))
        // Do any additional setup after loading the view.
   
        UITabBarItem.appearance().setTitleTextAttributes([NSFontAttributeName:  UIFont.systemFont(ofSize: 16, weight: UIFontWeightRegular) ], for: .normal)
      //  self.tabBarItem.setTitleTextAttributes([NSFontAttributeName:20], for: .normal)
    
    }
    override func viewDidAppear(_ animated: Bool) {
     //   UITabBar.appearance().selectionIndicatorImage =
        self.tabBar.selectionIndicatorImage = getImageWithColorPosition(color: UIColor.init(red: 120/255, green: 190/255, blue: 32/255, alpha: 1) , size: CGSize(width:(self.view.frame.size.width)/2,height: 49), lineSize: CGSize(width:(self.view.frame.size.width)/2, height:2))
     /*   if(self.selectedIndex == 0)
        {self.navigationController?.visibleViewController?.title = "Internal Announcements"
        }
        else if(self.selectedIndex == 1)
        {self.navigationController?.visibleViewController?.title = "Social Media"
        }
       */
        self.navigationController?.visibleViewController?.title = "Announcements"

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillLayoutSubviews() {
         (self.view.superview?.frame)!
        
        
        
        
        super.viewWillLayoutSubviews()
        
        
      //  self.view.frame = CGRect(x: 0, y: 0 , width: (self.view.superview?.frame.width)!, height: ((self.view.superview?.frame.height)!))
        Constants.tabbarHeight = Float(self.tabBar.frame.height)
        print(self.tabBar.frame.height)
        print("mark")
        var tabFrame:CGRect = self.tabBar.frame
        tabFrame.origin.y =  tabFrame.height;
        
        self.tabBar.frame = tabFrame
        
        
    }
    func getImageWithColorPosition(color: UIColor, size: CGSize, lineSize: CGSize) -> UIImage {
        let rect = CGRect(x:0, y: 0, width: size.width, height: size.height)
        let rectLine = CGRect(x:0, y:size.height-lineSize.height,width: lineSize.width,height: lineSize.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        UIColor.clear.setFill()
        UIRectFill(rect)
        color.setFill()
        UIRectFill(rectLine)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
