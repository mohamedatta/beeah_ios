//
//  Idea.swift
//  Beeah
//
//  Created by cloudymac on 6/11/17.
//  Copyright © 2017 cloudymac. All rights reserved.
//

import Foundation

class Idea {
    
    var date: String = ""
    var descriptions: String = ""
    var id: String = ""
    var ncomments: Int = 0
    
    var overAllRate: Float = 0.0
    
    var sortDate: Int64 = 0
    var title: String = ""
    var user: UserReference? = nil
    var usersRate = [UserRate]()
    var comments = [Comment]()
    init(aDict: [String: AnyObject]) {
        if let datetemp = aDict["date"]
        {self.date = datetemp as! String}
        if aDict["description"] != nil{
            self.descriptions = aDict["description"] as! String}
        self.overAllRate = aDict["overAllRate"] as! Float
        if aDict["sortDate"] != nil{
        
        self.sortDate = aDict["sortDate"] as! Int64 
        }
        if aDict["title"] != nil{
            self.title = aDict["title"] as! String
        }
        if let ideaid = aDict["ideaKey"]
        {
            self.id = ideaid as! String
        }
        if let ideaid = aDict["key"]
        {
            self.id = ideaid as! String
        }
        
        if aDict["user"] != nil{
            
        self.user = UserReference(aDict: aDict["user"] as! [String: AnyObject])
        }
        var rates=aDict["usersRate"] as? [String: Float]
        if(rates != nil){
        for (key, value) in (rates!)
        {
        
            self.usersRate.append(UserRate(uid: key,rate: value))
            }
            }
        var commentsJson=aDict["comments"] as? [Any]
        if(commentsJson != nil){
            for c in commentsJson!
            {
                
            self.comments.append(Comment(Dict: c as! [String : AnyObject]))
            }
            
        }
        
        }
    
    init() {
    }
    
        //self.usersRate
        
    
}
