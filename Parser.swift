//
//  Parser.swift
//  Beeah
//
//  Created by cloudymac on 6/16/17.
//  Copyright © 2017 cloudymac. All rights reserved.
//

import Foundation
class Parser
{
    
  
    public static  func parseIdeas(data: Data?) ->[Idea]
    {
        var ideas = [Idea]()
        do {
            if let data = data,
                let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                let ideasjson = json["items"] as? [[String: Any]] {
                for idea in ideasjson {
                    
                    ideas.append(Idea(aDict: idea as [String : AnyObject]))
                    
                }
            }
        } catch {
            print("Error deserializing JSON: \(error)")
        }
        
        return ideas
        
    }
  
    
    
    public static  func pareInternalAnnouncements(data: Data?) ->[Announcment]
    {
    var anns = [Announcment]()
        do {
            if let data = data,
                let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                let announcments = json["announcements"] as? [[String: Any]] {
                for anouncement in announcments {
                anns.append(Announcment(aDict: anouncement as [String : AnyObject]))
                    
                }
            }
        } catch {
            print("Error deserializing JSON: \(error)")
        }
        
    return anns
        
    }
  
    public static  func parseSocialMediaAnnouncements(data: Data?) ->[Announcment]
    {
        var anns = [SocialMediaAnnouncment]()
        do {
            if let data = data,
                let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                let announcments = json["externalannouncments"] as? [[String: Any]] {
                for anouncement in announcments {
                    anns.append(SocialMediaAnnouncment(aDict: anouncement as [String : AnyObject]))
                    
                }
            }
        } catch {
            print("Error deserializing JSON: \(error)")
            
        }
        
        return anns
        
    }

    
    public static  func parseeUserHappiness(data: Data?) ->Int
    {

        do {
            if let data = data,
                let json = try JSONSerialization.jsonObject(with: data) as? [String: Float],
                let happiness = json["overAllHappiness"]! as?  Float {
                return Int(happiness * 100)
            }
        } catch {
            print("Error deserializing JSON: \(error)")
        }
        
        return 0
        
    }
    
    
    
    public static  func parseTopContributers(data: Data?) ->[Contribution]
    {
        var topContributers = [Contribution]()
        
        do {
            if let data = data,
                let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                let contbs = json["items"] as? [[String: Any]] {
                for contribution in contbs {
                    topContributers.append(Contribution(Dict: contribution as [String : AnyObject]))
                    
                }
            }
        } catch {
            print("Error deserializing JSON: \(error)")
        }
        
        return topContributers
        
    }
    public static  func parseTopContributersV2(data: Data?) ->[(Int,[Contribution])]
    {
        
        var topContributers: [(Month: Int,Contribution: [Contribution])] = []
        

        do {
            if let data = data,
                let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                let contbsmonthly = json["items"] as? [[[String: Any]]] {
                
                for cont_in_month in contbsmonthly{
                    var contributionsInstance = [Contribution]()
                    
                    for contribution in cont_in_month {
                        contributionsInstance.append(Contribution(Dict: contribution as [String : AnyObject]))
                    }
                
                    topContributers.append((1,contributionsInstance))
         
                }
                
                
            }
        } catch {
            print("Error deserializing JSON: \(error)")
        }
        
        return topContributers
        
    }
    
    
    public static  func parsPolls(data: Data?) ->[Poll]
    {
        var polls = [Poll]()
        
        do {
            if let data = data,
                let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                let pollsdic = json["items"] as? [[String: Any]] {
                for polldic in pollsdic {
               polls.append(Poll(aDict: polldic as [String : AnyObject]))
                    
                    
                }
                
                
                
            }
        } catch {
            print("Error deserializing JSON: \(error)")
        }
        
        return polls
        
    }
    
    
    public static  func parseeHappiness(data: Data?) ->[Happiness]
    {
        var happinessV = [Happiness]()
        
        
        do {
            if let data = data,
                let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
                let happiness = json["items"]! as? [[String: Int]] {
                for happy in happiness {
                    happinessV.append(Happiness(aDict: happy as [String : Int]))
                }
            }
        } catch {
            print("Error deserializing JSON: \(error)")
        }
        
        return happinessV
        
    }
    


}
