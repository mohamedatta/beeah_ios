//
//  UserRate.swift
//  Beeah
//
//  Created by cloudymac on 6/12/17.
//  Copyright © 2017 cloudymac. All rights reserved.
//

import Foundation
class UserRate {

    var userId: String = ""
    var userRate: Float = 0
    
    init(uid:String , rate:Float ) {
        self.userId = uid
        self.userRate = rate
        
    }
    


}
