//
//  Contribution.swift
//  Beeah
//
//  Created by cloudymac on 6/27/17.
//  Copyright © 2017 cloudymac. All rights reserved.
//

import Foundation

class Contribution {
    
    var name: String = ""
    var email: String = ""
    var overAllHappiness = 0
    var overAllContribution = 0
    var id:String = ""
    var photoUrl: String = ""
    var imageData: Data? = nil
    var commenter: UserReference? = nil
    
    init(Dict: [String : AnyObject]) {
 
        self.name = Dict["name"] as! String
        self.email = Dict["email"] as! String
        
        
        if let temp = Dict["overAllHappiness"] as? Int
        {
            self.overAllHappiness = temp as! Int
        
        }
        
        self.overAllContribution = Dict["overAllContribution"] as! Int
        self.id = Dict["id"] as! String
        self.photoUrl = Dict["photoUrl"] as! String
        
        
    }
    
    
}
