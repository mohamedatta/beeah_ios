//
//  Comment.swift
//  Beeah
//
//  Created by cloudymac on 6/16/17.
//  Copyright © 2017 cloudymac. All rights reserved.
//

import Foundation
class Comment
{

    var comment: String = ""
    var date: String = ""
    var sortdate: Int64 = 0
    
    var commenter: UserReference? = nil
    
    init(Dict: [String : AnyObject]) {
        self.date = Dict["date"] as! String
        self.sortdate = Dict["sortDate"] as! Int64
        self.comment = Dict["comment"] as! String
        self.commenter =  UserReference(aDict: Dict["commenter"] as! [String : AnyObject])        
        
    }
    
    
}
