//
//  UserReference.swift
//  Beeah
//
//  Created by cloudymac on 6/11/17.
//  Copyright © 2017 cloudymac. All rights reserved.
//

import Foundation
class UserReference {
    
    var userId:String=""
    var userName:String=""
    
    init(aDict: [String: AnyObject]) {
        self.userId = aDict["userId"] as! String
        self.userName = aDict["userName"] as! String
        
    }
    
   
}
