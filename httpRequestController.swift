//
//  QuestionController.swift
//  Beeah
//
//  Created by cloudymac on 6/9/17.
//  Copyright © 2017 cloudymac. All rights reserved.
//


import Foundation
import SwiftHTTP
import FirebaseAuth
class httpRequestController: NSObject {
    
    
    func request( url:String , type:String , completion: @escaping ((Data) -> ())) -> Void {
        
        
        var url = url
            
            do {
 
                var opt:HTTP? = nil;
                
                if(type=="Get"){
                    opt = try HTTP.GET(url)}
                else if(type=="Post"){
                opt = try HTTP.POST(url)
                 
                    
                }
                else if(type=="PUT")
                {opt = try HTTP.PUT(url)}
                
                opt?.start { response in
                    if let err = response.error {
                        print("error: \(err.localizedDescription)")

                        completion(response.data)
                        return //also notify app of failure as needed
                    }
                    completion(response.data)
                    
                    //print("data is: \(response.data)") access the response of the data with response.data
                }
            } catch let error {
                print("got an error creating the request: \(error)")
            }
        
        

        
       
    }
    
    func test()
    {
    print("fired")
    }
}
